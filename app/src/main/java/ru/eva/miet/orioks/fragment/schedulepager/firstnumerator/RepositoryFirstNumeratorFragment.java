package ru.eva.miet.orioks.fragment.schedulepager.firstnumerator;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.model.schedule.Data;


public class RepositoryFirstNumeratorFragment implements ContractFirstNumeratorFragment.Repository {
    @Override
    public List<Data> getSchedule(int week, String group) {
        return StorageHelper.getInstance().getSchedulersDataCurrentWeek(week, group);
    }
}
