package ru.eva.miet.orioks.interfaces.data;

public interface OnGroupsReceived {
    void onResponse(String[] groupList);

    void onFailure(String message);
}
