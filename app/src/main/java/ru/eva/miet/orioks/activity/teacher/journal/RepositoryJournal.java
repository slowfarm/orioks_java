package ru.eva.miet.orioks.activity.teacher.journal;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnJournalReceived;
import ru.eva.miet.orioks.model.orioks.teacher.Journal;
import ru.eva.miet.orioks.model.orioks.teacher.JournalStudent;

class RepositoryJournal implements ContractJournal.Repository {
    @Override
    public void getJournal(int disciplineId, int groupId, OnJournalReceived onJournalReceived) {
        NetworkHelper.getInstance().getJournal(StorageHelper.getInstance().getAccessToken(), disciplineId, groupId, onJournalReceived);
    }

    @Override
    public void setJournal(Journal journal) {
        StorageHelper.getInstance().setJournal(journal);
    }

    @Override
    public List<JournalStudent> getLocalJournal(int groupId) {
        return StorageHelper.getInstance().getJournal(groupId);
    }
}
