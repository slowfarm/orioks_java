package ru.eva.miet.orioks.model.orioks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class AccessToken extends RealmObject {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("currentRole")
    @Expose
    private String currentRole;
    @SerializedName("roles")
    @Expose
    private RealmList<Role> roles = null;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }

    public RealmList<Role> getRoles() {
        return roles;
    }

    public void setRoles(RealmList<Role> roles) {
        this.roles = roles;
    }

}
