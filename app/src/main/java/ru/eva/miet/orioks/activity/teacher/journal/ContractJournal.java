package ru.eva.miet.orioks.activity.teacher.journal;


import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnJournalReceived;
import ru.eva.miet.orioks.model.orioks.teacher.Journal;
import ru.eva.miet.orioks.model.orioks.teacher.JournalStudent;

class ContractJournal {
    interface View {
        void setToolbarTitle(String  toolbarTitle);

        void setAdapter(List<JournalStudent> students);

        void showToast(String text);
    }

    interface Presenter {

        void setToolbarTitle(int groupId);

        void getEvents(int disciplineId, int groupId);
    }

    interface Repository {

        void getJournal(int disciplineId, int groupId, OnJournalReceived onJournalReceived);

        void setJournal(Journal journal);

        List<JournalStudent> getLocalJournal(int groupId);
    }
}