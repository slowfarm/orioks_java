package ru.eva.miet.orioks.activity.main;


import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;

import androidx.appcompat.app.AppCompatDelegate;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnStudentReceived;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.schedule.WeekType;

class PresenterMain implements ContractMain.Presenter, OnStudentReceived {

    private ContractMain.View mView;
    private ContractMain.Repository mRepository;

    PresenterMain(ContractMain.View mView) {
        this.mView = mView;
        this.mRepository = new RepositoryMain();
    }

    @Override
    public void setTypeOfWeek() {
        WeekType weekType = mRepository.getCurrentWeekType();
        int weekIndicator = ConvertHelper.getInstance().getCurrentWeekIndicator(weekType.getSemesterStart());
        String week = ConvertHelper.getInstance().getCurrentValue(weekIndicator);
        weekIndicator++;
        float progress = (float) weekIndicator / 18 * 100;
        mView.setWeekType(String.valueOf(weekIndicator), week, progress);
    }

    @Override
    public void changeTheme(Context context, boolean isChecked) {
        AppCompatDelegate.setDefaultNightMode(isChecked ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
        mRepository.setIsNightThemeSelected(context, isChecked);
    }

    @Override
    public void initSwitchThemeItem(Context context) {
        mView.initSwitchThemeItem(mRepository.getIsDarkThemeSelected(context));
    }

    @Override
    public void checkRoles(Context context) {
        String currentRole = mRepository.getCurrentRole();
        if (currentRole.contains("teacher")) {
            getTeacher();
        }
        if (currentRole.contains("student")) {
            getStudent();
        }
    }


    public void getStudent() {
        mView.initStudentViews();
        Student student = mRepository.getLocalStudent();
        if (student == null || student.getFullName() == null) {
            mRepository.getStudent(this);
        } else {
            mView.setPerson(student.getFullName(), student.getRecordBookId(), student.getGroup().getShortName());
        }
    }

    private void getTeacher() {
        mView.initTeacherViews();
        Student student = mRepository.getLocalStudent();
        if (student == null || student.getFullName() == null) {
            mRepository.getTeacher(this);
        } else {
            mView.setPerson(student.getFullName(), student.getInstitute().getShortName(), "");
        }
    }

    @Override
    public void onResponse(Student student) {
        if (student != null) {
            String currentRole = mRepository.getCurrentRole();
            mRepository.setStudent(student);
            if (currentRole.contains("teacher")) {
                mView.setPerson(student.getFullName(), student.getInstitute().getShortName(), "");
            }
            if (currentRole.contains("student")) {
                mView.setPerson(student.getFullName(), student.getRecordBookId(), student.getGroup().getShortName());
            }
        }
    }

    @Override
    public void onFailure(String message) {
        mView.showToast(message);
    }
}