package ru.eva.miet.orioks.interfaces.view;

public interface OnEventItemClickListener {
    void onClick(int disciplineId, int groupId);
    void scrollToBottom();
}
