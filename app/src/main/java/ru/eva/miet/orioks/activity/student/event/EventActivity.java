package ru.eva.miet.orioks.activity.student.event;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.model.orioks.student.Discipline;
import ru.eva.miet.orioks.model.orioks.student.Event;

public class EventActivity extends AppCompatActivity implements ContractEvent.View {

    private ContractEvent.Presenter mPresenter;
    private LinearLayout eventContainerLayout;
    private View itemEventWeek;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Intent intent = getIntent();
        String intExtraName = "id";
        int id = intent.getIntExtra(intExtraName, 0);
        eventContainerLayout = findViewById(R.id.event_container_layout);

        mPresenter = new PresenterEvent(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        mPresenter.setToolbarTitle(id);

        mPresenter.setHeaderData(id);
        mPresenter.getEvents(id);
    }

    @Override
    public void setToolbarTitle(String toolbarTitle) {
        AppCompatTextView disciplineTitleTextView = findViewById(R.id.discipline_title_text_view);
        disciplineTitleTextView.setText(toolbarTitle);
    }

    @Override
    public void setHeaderData(Discipline discipline) {
        ((AppCompatTextView)findViewById(R.id.type_text_view)).setText(discipline.getFormControl());
        ((AppCompatTextView)findViewById(R.id.teachers_text_view)).setText(discipline.getTeachersByString());
        ((AppCompatTextView)findViewById(R.id.department_text_view)).setText(discipline.getInstitute());
    }

    @Override
    public void clearListView() {
        eventContainerLayout.removeAllViews();
    }

    @Override
    public void setNoEventTextViewVisibility(boolean visibility) {
        findViewById(R.id.no_event_text_view).setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void addWeek(Integer week, List<Event> eventList) {
            itemEventWeek = getLayoutInflater().inflate(R.layout.item_event_week, null);
            ((AppCompatTextView) itemEventWeek.findViewById(R.id.week_text_view)).setText(week.toString());
            eventContainerLayout.addView(itemEventWeek);
            mPresenter.addEvents(week, eventList);
    }

    @Override
    public void addEvents(Event event) {
        View itemEvent = getLayoutInflater().inflate(R.layout.item_event, null);
        ((AppCompatTextView) itemEvent.findViewById(R.id.discipline_text_view)).setText(event.getType());
        ((AppCompatTextView) itemEvent.findViewById(R.id.name_text_view)).setText(event.getName());
        ((AppCompatTextView) itemEvent.findViewById(R.id.rank_current_text_view)).setText(event.getCurrentGradeInString());
        ((AppCompatTextView) itemEvent.findViewById(R.id.rank_current_text_view)).setTextColor(event.getColor());
        ((AppCompatTextView) itemEvent.findViewById(R.id.rank_max_text_view)).setText(event.getMaxGradeInString());
        ((LinearLayout) itemEventWeek.findViewById(R.id.events_list_linear_layout)).addView(itemEvent);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


}
