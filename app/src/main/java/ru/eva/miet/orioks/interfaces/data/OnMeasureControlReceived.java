package ru.eva.miet.orioks.interfaces.data;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;

public interface OnMeasureControlReceived {
    void onResponse(List<MeasureControl> measureControlList, int disciplineId, int studentId);

    void onFailure(String text);
}
