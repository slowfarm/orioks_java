package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.orioks.Roles;

public interface OnRolesReceived {
    void onResponse(Roles roles);
    void onFailure(String message);
}
