package ru.eva.miet.orioks.fragment.schedulepager.firstnumerator;

import java.util.List;

import ru.eva.miet.orioks.model.schedule.Data;


class ContractFirstNumeratorFragment {

    interface View {

        void setAdapter(List<Data> schedule);
    }

    interface Presenter {

        void getSchedule(String group);
    }

    interface Repository {

        List<Data> getSchedule(int week, String group);
    }
}
