package ru.eva.miet.orioks.fragment.student.discipline;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnDisciplinesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.student.Discipline;


class RepositoryDiscipline implements ContractDiscipline.Repository {
    @Override
    public List<Discipline> getDisciplineList() {
        return StorageHelper.getInstance().getDisciplines();
    }

    @Override
    public void setDisciplineList(OnDisciplinesReceived onDisciplinesReceived) {
        NetworkHelper.getInstance().getDisciplines(StorageHelper.getInstance().getAccessToken(), onDisciplinesReceived);
    }

    @Override
    public void setDisciplineList(List<Discipline> disciplineList) {
        StorageHelper.getInstance().setDisciplines(disciplineList);
    }

    @Override
    public void clearAllTables() {
        StorageHelper.getInstance().clearTables();
    }

    @Override
    public int getDisciplineId(int position) {
        return StorageHelper.getInstance().getDisciplines().get(position).getId();
    }

    @Override
    public Semester getLocalSemester() {
        return StorageHelper.getInstance().getSemester();
    }

    @Override
    public void changeSemester(Integer id, OnSemestersReceived onSemestersReceived) {
        NetworkHelper.getInstance().changeSemester(StorageHelper.getInstance().getAccessToken(), id, onSemestersReceived);
    }

    @Override
    public void clearDisciplines() {
        StorageHelper.getInstance().clearDisciplines();
    }

    @Override
    public void deleteToken(OnTokenReceived onTokenReceived) {
        NetworkHelper.getInstance().deleteAccessToken(StorageHelper.getInstance().getAccessToken(), -1, onTokenReceived);
    }
}
