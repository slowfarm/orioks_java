package ru.eva.miet.orioks.fragment.schedule;

import android.content.Context;

import java.util.List;


class ContractSchedule {
    interface View {
        void setRecyclerView(List<String> groupList, int visibility);

        void notifyDataSetChanged(List<String> localGroups);

        void showToast(String text);
    }

    interface Presenter {

        void getRecyclerView();

        void removeGroup(String group);

        void addLocalGroup(String group);

        void addPinnedShortcut(Context context, String group);
    }

    interface Repository {
        List<String> getLocalGroups();

        void removeGroup(String group);

        void addLocalGroup(String group);
    }
}
