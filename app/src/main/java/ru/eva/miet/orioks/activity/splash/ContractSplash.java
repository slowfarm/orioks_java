package ru.eva.miet.orioks.activity.splash;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.interfaces.data.OnWeekTypeReceived;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.schedule.WeekType;

class ContractSplash {
    interface View {
        void startMainActivity();

        void startAuthActivity();

        void showToast(String error);
    }

    interface Presenter {
        void checkAccessToken();
    }

    interface Repository {
        String getAccessToken();

        void setStudent(Student student);

        void deleteToken(OnTokenReceived onTokenReceived);

        void clearAllTables();

        void getAllActiveTokens(OnAllAccessTokensReceived onAllAccessTokensReceived);

        void setAllActiveTokens(List<Security> tokens);

        WeekType getCurrentWeekType();

        void getTypeOfWeek(OnWeekTypeReceived onWeekTypeReceived);

        void setWeekType(WeekType weekType);

        void getRoles(OnRolesReceived onRolesReceived);

        void setRoles(Roles roles);

        void getSemester(OnSemestersReceived onSemestersReceived);

        void setSemester(Semester semester);
    }
}
