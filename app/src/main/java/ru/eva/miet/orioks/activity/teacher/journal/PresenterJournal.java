package ru.eva.miet.orioks.activity.teacher.journal;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnJournalReceived;
import ru.eva.miet.orioks.model.orioks.teacher.Journal;
import ru.eva.miet.orioks.model.orioks.teacher.JournalStudent;

class PresenterJournal implements ContractJournal.Presenter, OnJournalReceived {
    private ContractJournal.View mView;
    private ContractJournal.Repository mRepository;

    PresenterJournal(ContractJournal.View mView) {
        this.mView = mView;
        mRepository = new RepositoryJournal();
    }

    @Override
    public void setToolbarTitle(int groupId) {
        mView.setToolbarTitle(StorageHelper.getInstance().getGroup(groupId));
    }

    @Override
    public void getEvents(int disciplineId, int groupId) {
        mView.setAdapter(ConvertHelper.getInstance().ConvertJournalStudents(
                mRepository.getLocalJournal(groupId)));
        mRepository.getJournal(disciplineId, groupId, this);
    }

    @Override
    public void onResponse(Journal journal, int groupId) {
        if (journal.getStudents() != null) {
            for(JournalStudent student : journal.getStudents())
                student.setGroupId(groupId);
            mRepository.setJournal(journal);
            mView.setAdapter(ConvertHelper.getInstance().ConvertJournalStudents(journal.getStudents()));
        }
    }

    @Override
    public void onFailure(String text) {
        mView.showToast(text);
    }

}
