package ru.eva.miet.orioks.fragment.student.debt;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnDebtsReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.student.Debt;


class ContractDebt {
    interface View {

        void setRecyclerView(List<Debt> debtList);

        void addRecyclerVIewItems(List<Debt> debtList, int visibility);

        void showToast(String text);

        void unsetRefreshing();

        void finishActivity();

        void startEventActivity(int id);
    }

    interface Presenter {

        void getDebtList();

        void setDebtList();

        void onDebtClick(int position);
    }

    interface Repository {

        List<Debt> getDebtList();

        void setDebtList(OnDebtsReceived onDebtsReceived);

        void setDebtList(List<Debt> debtList);

        void deleteToken(OnTokenReceived onTokenReceived);

        void clearAllTables();

        int getDebtId(int position);
    }
}
