package ru.eva.miet.orioks.activity.auth;

import android.content.Context;
import android.provider.Settings;
import android.text.Editable;
import android.util.Base64;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnPusherReceived;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.interfaces.data.OnWeekTypeReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.schedule.WeekType;

public class PresenterAuth implements ContractAuth.Presenter,
        OnTokenReceived,
        OnAllAccessTokensReceived,
        OnPusherReceived,
        OnWeekTypeReceived,
        OnRolesReceived,
        OnSemestersReceived {

    private ContractAuth.View mView;
    private ContractAuth.Repository mRepository;
    private boolean isStudentNumberValid = false;
    private boolean isLoginValid = false;

    PresenterAuth(ContractAuth.View mView) {
        this.mView = mView;
        mRepository = new RepositoryAuth();
    }

    @Override
    public void onStudentNumberTextChanged(Editable text) {
        int passwordLength = text.toString().length();
        boolean isValid = passwordLength >= 6 && passwordLength <= 8;
        isStudentNumberValid = isValid;
        mView.setLoginButtonEnable(isStudentNumberValid && isLoginValid);
        if (isValid)
            mView.setStudentNumberLayoutError(null);
        else
            mView.setStudentNumberLayoutError("Длина должна составлять 6-8 символов");

    }

    @Override
    public void onPasswordTextChangeListener(Editable text) {
        boolean isValid = text.toString().length() > 0;
        isLoginValid = isValid;
        mView.setLoginButtonEnable(isStudentNumberValid && isLoginValid);
        if (isValid)
            mView.setLoginLayoutError(null);
        else
            mView.setLoginLayoutError("Поле не должно быть пустым");
    }

    @Override
    public void setLogin(Context context) {
        String login = mRepository.getLogin(context);
        if (login != null)
            mView.setLogin(login);
    }

    @Override
    public void onLoginButtonClick(Editable login, Editable password, Context context) {
        mRepository.getAccessToken(encodeString(login, password), this);
        mRepository.setLogin(login.toString(), context);
        mView.setLoginButtonEnable(false);
    }


    @Override
    public void onResponse(AccessToken accessToken, int position) {
        mRepository.setAccessToken(accessToken);
        mRepository.getAllActiveTokens(this);
    }

    @Override
    public void onResponse(List<Security> tokens) {
        mRepository.setAllActiveTokens(tokens);
        mRepository.getTypeOfWeek(this);
    }

    @Override
    public void onResponse(WeekType typeOfWeek) {
        mRepository.setTypeOfWeek(typeOfWeek);
        mRepository.getRoles(this);

    }


    @Override
    public void onResponse(Roles roles) {
        mRepository.setRoles(roles);
        mRepository.getSemesters(this);
    }

    @Override
    public void onResponse(Semester semester) {
        if(semester != null) {
            mRepository.setSemester(semester);
        }
        if(!mRepository.getLocalRoles().getParent().contains("teacher")) {
            mView.showPusherDialog();
        } else {
            mView.startActivity();
        }
    }

    @Override
    public void allowPushService(Context context) {
        RequestBody requestBody = new RequestBody();
        requestBody.setDeviceUuid(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        requestBody.setDeviceType(0);
        requestBody.setPushKey(mRepository.getFirebaseToken(context));
        mRepository.registerPusher(requestBody, this);
    }

    @Override
    public void onResponse() {
        mView.startActivity();
    }

    @Override
    public void onFailure() {
        mView.startActivity();
    }

    @Override
    public void onFailure(String message) {
        mRepository.deleteAccessToken();
        mView.showToast(message);
        mView.setLoginButtonEnable(true);
    }

    private String encodeString(Editable login, Editable password) {
        return "Basic " + Base64.encodeToString((login.toString().replaceAll("[^\\d.]", "") + ":" + password.toString()).getBytes(), Base64.NO_WRAP);
    }
}
