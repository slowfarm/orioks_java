package ru.eva.miet.orioks.interfaces.view;

public interface OnRoleItemClickListener {
    void onClick(String role);
}
