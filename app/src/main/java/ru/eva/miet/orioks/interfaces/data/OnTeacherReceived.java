package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.orioks.student.Student;

public interface OnTeacherReceived {
    void onResponse(Student student);

    void onFailure(String message);
}
