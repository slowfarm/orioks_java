package ru.eva.miet.orioks.activity.student.resit;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnResitsReceived;
import ru.eva.miet.orioks.model.orioks.student.Debt;
import ru.eva.miet.orioks.model.orioks.student.Resit;

class ContractResit {
    interface View {
        void setToolbarTitle(String toolbarTitle);

        void setHeaderData(Debt debt);

        void setRecyclerView(List<Resit> resitList);

        void showToast(String text);
    }

    interface Presenter {
        void setToolbarTitle(int id);

        void setHeaderData(int id);

        void getResits(int id);
    }

    interface Repository {

        String getToolbarTitle(int id);

        Debt getDebt(int id);

        List<Resit> getResits(int id);

        void getResitList(int id, OnResitsReceived onResitsReceived);

        void setResitList(List<Resit> resitList, int id);
    }
}
