package ru.eva.miet.orioks.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.widget.Toast;

import ru.eva.miet.orioks.activity.main.MainActivity;
import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.auth.AuthActivity;

public class SplashActivity extends AppCompatActivity implements ContractSplash.View {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ContractSplash.Presenter mPresenter = new PresenterSplash(this);
        mPresenter.checkAccessToken();
    }

    @Override
    public void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void startAuthActivity() {
        startActivity(new Intent(this, AuthActivity.class));
        finish();
    }

    @Override
    public void showToast(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
