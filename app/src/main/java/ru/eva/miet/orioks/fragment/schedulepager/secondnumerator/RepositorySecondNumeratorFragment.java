package ru.eva.miet.orioks.fragment.schedulepager.secondnumerator;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.model.schedule.Data;


public class RepositorySecondNumeratorFragment implements ContractSecondNumeratorFragment.Repository {
    @Override
    public List<Data> getSchedule(int week, String group) {
        return StorageHelper.getInstance().getSchedulersDataCurrentWeek(week, group);
    }
}
