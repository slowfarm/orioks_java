package ru.eva.miet.orioks.fragment.roles;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnRoleChangeReceiver;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.RolesList;

class ContractRoles {
    interface View {

        void showToast(String text);

        void setAdapter(List<RolesList> rolesList);

        void finishActivity();

        void unsetRefreshing();

        void startSplashActivity();
    }

    interface Presenter {

        void getLocalRoles();

        void changeRole(String role);

        void refreshRoles();
    }

    interface Repository {

        void clearAllTables();

        Roles getLocalRoles();

        void changeRole(String role, OnRoleChangeReceiver onRoleChangeReceiver);

        void getRoles(OnRolesReceived onRolesReceived);

        void setRoles(Roles roles);

        void clearDisciplines();
    }
}
