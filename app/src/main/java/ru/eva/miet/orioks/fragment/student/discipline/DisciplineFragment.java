package ru.eva.miet.orioks.fragment.student.discipline;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.auth.AuthActivity;
import ru.eva.miet.orioks.activity.splash.SplashActivity;
import ru.eva.miet.orioks.activity.student.event.EventActivity;
import ru.eva.miet.orioks.adapter.student.DisciplineFragmentAdapter;
import ru.eva.miet.orioks.interfaces.view.OnDisciplineItemClickListener;
import ru.eva.miet.orioks.model.orioks.student.Discipline;

public class DisciplineFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        ContractDiscipline.View,
        OnDisciplineItemClickListener {

    private DisciplineFragmentAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ContractDiscipline.Presenter mPresenter;

    private View view;

    private RapidFloatingActionLayout fabLayout;
    private RapidFloatingActionButton fab;
    private RapidFloatingActionHelper helper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_discipline, container, false);

        Resources resources = getResources();
        int orange = resources.getColor(R.color.grade_orange);
        int yellow = resources.getColor(R.color.grade_amber);
        int lightGreen = resources.getColor(R.color.grade_lime);
        int green = resources.getColor(R.color.grade_green);

        fabLayout = view.findViewById(R.id.fab_layout);
        fab = view.findViewById(R.id.fab);

        swipeRefreshLayout = view.findViewById(R.id.container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(orange, yellow, lightGreen, green);

        mPresenter = new PresenterDiscipline(this);
        mPresenter.getDisciplineList();
        mPresenter.setDisciplineList();

        mPresenter.getSemesters();

        return view;
    }

    @Override
    public void onRefresh() {
        mPresenter.setDisciplineList();
    }

    @Override
    public void setRecyclerView(List<Discipline> disciplineList) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        adapter = new DisciplineFragmentAdapter(disciplineList);
        adapter.setOnDisciplineItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void addRecyclerViewItems(List<Discipline> disciplineList, int visibility) {
        adapter.addItems(disciplineList);
        view.findViewById(R.id.no_discipline_text_view).setVisibility(visibility);
    }

    @Override
    public void setSemestersAdapter(List<RFACLabelItem> localSemester) {
        RapidFloatingActionContentLabelList rfaContent = new RapidFloatingActionContentLabelList(view.getContext());
        rfaContent.setItems(localSemester);
        rfaContent.setOnRapidFloatingActionContentLabelListListener(
                new RapidFloatingActionContentLabelList.OnRapidFloatingActionContentLabelListListener() {
                    @Override
                    public void onRFACItemLabelClick(int position, RFACLabelItem item) {
                        helper.toggleContent();
                        mPresenter.onFabClick(position);
                    }
                    @Override
                    public void onRFACItemIconClick(int position, RFACLabelItem item) { }
                });
        helper = new RapidFloatingActionHelper(view.getContext(), fabLayout, fab, rfaContent).build();
    }

    @Override
    public void startSplashActivity() {
        ((Activity)view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), SplashActivity.class));
    }

    @Override
    public void hideSemesterFab() {
        fab.setVisibility(View.GONE);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(view.getContext(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void unsetRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void finishActivity() {
        ((Activity)view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), AuthActivity.class));
    }

    @Override
    public void startEventActivity(int id) {
        view.getContext().startActivity(new Intent(view.getContext(), EventActivity.class).putExtra("id", id));
    }

    @Override
    public void onClick(int position) {
        mPresenter.onDisciplineClick(position);
    }
}