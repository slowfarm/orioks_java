package ru.eva.miet.orioks.interfaces.data;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.student.Discipline;

public interface OnDisciplinesReceived {
    void onResponse(List<Discipline> disciplineList);

    void onFailure(String message);
}
