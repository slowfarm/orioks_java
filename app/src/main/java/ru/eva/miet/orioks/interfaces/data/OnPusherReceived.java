package ru.eva.miet.orioks.interfaces.data;

public interface OnPusherReceived {
    void onResponse();
    void onFailure();
}
