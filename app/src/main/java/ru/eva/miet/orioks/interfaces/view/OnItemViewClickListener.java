package ru.eva.miet.orioks.interfaces.view;

import android.view.View;

import ru.eva.miet.orioks.model.orioks.Security;

public interface OnItemViewClickListener {
    void onClick(Security token, int position, View view);
}
