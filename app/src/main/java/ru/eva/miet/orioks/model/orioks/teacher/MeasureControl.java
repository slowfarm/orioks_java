package ru.eva.miet.orioks.model.orioks.teacher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class MeasureControl extends RealmObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("week")
    @Expose
    private Integer week;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("maxScore")
    @Expose
    private Double maxScore;
    @SerializedName("currentScore")
    @Expose
    private Double currentScore;
    private String currentGradeInString;
    private int color;
    private String maxGradeInString;
    @SerializedName("studentId")
    private int studentId;
    @SerializedName("disciplineId")
    private int disciplineId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }

    public Double getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(Double currentScore) {
        this.currentScore = currentScore;
    }

    public void setCurrentGradeInString(String currentGradeInString) {
        this.currentGradeInString = currentGradeInString;
    }

    public String getCurrentGradeInString() {
        return currentGradeInString;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public void setMaxGradeInString(String maxGradeInString) {
        this.maxGradeInString = maxGradeInString;
    }

    public String getMaxGradeInString() {
        return maxGradeInString;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getGroupId() {
        return disciplineId;
    }

    public void setGroupId(int disciplineId) {
        this.disciplineId = disciplineId;
    }
}
