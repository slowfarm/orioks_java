package ru.eva.miet.orioks.activity.student.resit;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnResitsReceived;
import ru.eva.miet.orioks.model.orioks.student.Debt;
import ru.eva.miet.orioks.model.orioks.student.Resit;

class RepositoryResit implements ContractResit.Repository {

    @Override
    public String getToolbarTitle(int id) {
        return StorageHelper.getInstance().getDebt(id).getName();
    }

    @Override
    public Debt getDebt(int id) {
        return StorageHelper.getInstance().getDebt(id);
    }

    @Override
    public List<Resit> getResits(int id) {
        return StorageHelper.getInstance().getResitList(id);
    }

    @Override
    public void getResitList(int id, OnResitsReceived onResitsReceived) {
        NetworkHelper.getInstance().getResits(StorageHelper.getInstance().getAccessToken(), id, onResitsReceived);
    }

    @Override
    public void setResitList(List<Resit> resitList, int id) {
        StorageHelper.getInstance().setResits(resitList, id);
    }
}
