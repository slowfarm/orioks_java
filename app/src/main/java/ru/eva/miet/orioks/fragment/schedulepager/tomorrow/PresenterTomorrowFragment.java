package ru.eva.miet.orioks.fragment.schedulepager.tomorrow;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.model.schedule.Data;


class PresenterTomorrowFragment implements ContractTomorrowFragment.Presenter {
    private ContractTomorrowFragment.View mView;
    private ContractTomorrowFragment.Repository mRepository;


    PresenterTomorrowFragment(ContractTomorrowFragment.View mView) {
        this.mView = mView;
        mRepository = new RepositoryTomorrowFragment();
    }


    @Override
    public void getSchedule(String group) {

        int currentDayOfWeek = ConvertHelper.getInstance().getNextDayOfWeek();
        int currentWeek = ConvertHelper.getInstance().getCurrentWeek(
                mRepository.getCurrentWeekType().getSemesterStart());
        if (currentDayOfWeek == 1)
            currentWeek++;
        if (currentWeek == 4)
            currentWeek = 0;
        List<Data> dataList = ConvertHelper.getInstance().scheduleDay(
                mRepository.getSchedule(currentWeek, currentDayOfWeek, group));
        mView.setAdapter(dataList);
    }
}
