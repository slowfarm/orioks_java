package ru.eva.miet.orioks.fragment.schedulepager.seconddenominator;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.model.schedule.Data;


public class PresenterSecondDenominationFragment implements ContractSecondDenominationFragment.Presenter{
    private ContractSecondDenominationFragment.View mView;
    private ContractSecondDenominationFragment.Repository mRepository;


    PresenterSecondDenominationFragment(ContractSecondDenominationFragment.View mView) {
        this.mView = mView;
        mRepository = new RepositorySecondDenominationFragment();
    }

    @Override
    public void getSchedule(String group) {
        List<Data> dataList = ConvertHelper.getInstance().scheduleWeek(mRepository.getSchedule(3, group));
        mView.setAdapter(dataList);
    }
}
