package ru.eva.miet.orioks.activity.group;


import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnGroupsReceived;

class ContractGroupActivity {
    interface View {

        void showToast(String message);

        void setRecyclerView(List<String> groupList);
    }

    interface Presenter {


        void getGroups();
    }

    interface Repository {
        void getGroups(OnGroupsReceived onGroupsReceived);
    }
}
