package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.orioks.AccessToken;

public interface OnTokenReceived {
    void onResponse(AccessToken accessToken, int position);

    void onFailure(String message);
}
