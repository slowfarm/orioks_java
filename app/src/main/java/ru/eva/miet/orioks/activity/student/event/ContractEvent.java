package ru.eva.miet.orioks.activity.student.event;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnEventsReceived;
import ru.eva.miet.orioks.model.orioks.student.Discipline;
import ru.eva.miet.orioks.model.orioks.student.Event;

class ContractEvent {
    interface View {
        void setToolbarTitle(String toolbarTitle);

        void setHeaderData(Discipline event);

        void addWeek(Integer week, List<Event> eventList);

        void showToast(String text);

        void addEvents(Event event);

        void clearListView();

        void setNoEventTextViewVisibility(boolean visibility);
    }

    interface Presenter {
        void setToolbarTitle(int id);

        void setHeaderData(int id);

        void getEvents(int id);

        void addEvents(Integer week, List<Event> eventList);
    }

    interface Repository {

        String getToolbarTitle(int id);

        Discipline getDiscipline(int id);

        List<Event> getEvents(int id);

        void getEventList(int id, OnEventsReceived onEventsReceived);

        void setEventList(List<Event> eventsList, int id);
    }
}
