package ru.eva.miet.orioks.activity.group;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.interfaces.data.OnGroupsReceived;

class RepositoryGroupActivity implements ContractGroupActivity.Repository {

    @Override
    public void getGroups(OnGroupsReceived onGroupsReceived) {
        NetworkHelper.getInstance().getGroups(onGroupsReceived);
    }
}
