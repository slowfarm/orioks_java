package ru.eva.miet.orioks.adapter.schedule;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnGroupItemClickListener;

public class SchedulerFragmentAdapter extends RecyclerView.Adapter<SchedulerFragmentAdapter.ViewHolder> {

    private List<String> groupList;
    private OnGroupItemClickListener onGroupItemClickListener;

    public SchedulerFragmentAdapter(List<String> groupList) {
        this.groupList = groupList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.group_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String group = groupList.get(position);
        holder.groupTextView.setText(group);
    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }

    public void addItems(List<String> groupList) {
        this.groupList = groupList;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        groupList.remove(position);
        notifyItemRemoved(position);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView groupTextView;
        private final TextView addToDesktopTextView;

        ViewHolder(View itemView) {
            super(itemView);
            groupTextView = itemView.findViewById(R.id.group_text_view);
            addToDesktopTextView = itemView.findViewById(R.id.add_to_desktop_text_view);
            addToDesktopTextView.setOnClickListener(v ->
                    onGroupItemClickListener.onShortcutAddClick(groupList.get(getAdapterPosition())));
            itemView.setOnClickListener(v -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION)
                    onGroupItemClickListener.onClick(groupList.get(getAdapterPosition()));
            });
            itemView.setOnLongClickListener(v -> {
                onGroupItemClickListener.onLongClick(getAdapterPosition(), groupList.get(getAdapterPosition()));
                return false;
            });
        }
    }

    public void setOnGroupItemClickListener(OnGroupItemClickListener onGroupItemClickListener) {
        this.onGroupItemClickListener = onGroupItemClickListener;
    }
}
