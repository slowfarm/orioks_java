package ru.eva.miet.orioks.interfaces.data;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.student.Resit;

public interface OnResitsReceived {
    void onResponse(List<Resit> resitList, int id);

    void onFailure(String message, int id);
}
