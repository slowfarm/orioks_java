package ru.eva.miet.orioks.fragment.schedulepager.seconddenominator;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.adapter.schedule.SchedulerAdapter;
import ru.eva.miet.orioks.model.schedule.Data;

public class SecondDenominatorFragment extends Fragment implements ContractSecondDenominationFragment.View {

    RecyclerView recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewItem = inflater.inflate(R.layout.scheduler_pager_item, container, false);

        String group = getArguments().getString("group","");

        recyclerView = viewItem.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(viewItem.getContext()));

        ContractSecondDenominationFragment.Presenter mPresenter = new PresenterSecondDenominationFragment(this);
        mPresenter.getSchedule(group);

        return viewItem;
    }

    @Override
    public void setAdapter(List<Data> schedule) {
        SchedulerAdapter adapter = new SchedulerAdapter(schedule);
        recyclerView.setAdapter(adapter);
    }
}