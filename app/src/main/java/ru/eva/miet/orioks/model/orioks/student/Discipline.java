package ru.eva.miet.orioks.model.orioks.student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Discipline extends RealmObject {

    @SerializedName("formControl")
    @Expose
    private String formControl;
    @SerializedName("score")
    @Expose
    private Double score;
    @SerializedName("institute")
    @Expose
    private String institute;
    @SerializedName("examDate")
    @Expose
    private String examDate;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("currentMaxScore")
    @Expose
    private Double currentMaxScore;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("teachers")
    @Expose
    private RealmList<String> teachers = null;
    private int color;
    private String rank;
    private String teachersByString;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getFormControl() {
        return formControl;
    }

    public void setFormControl(String formControl) {
        this.formControl = formControl;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getExamDate() {
        return examDate;
    }

    public void setExamDate(String examDate) {
        this.examDate = examDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Double getCurrentMaxScore() {
        return currentMaxScore;
    }

    public void setCurrentMaxScore(Double currentMaxScore) {
        this.currentMaxScore = currentMaxScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<String> getTeachers() {
        return teachers;
    }

    public void setTeachers(RealmList<String> teachers) {
        this.teachers = teachers;
    }

    public String getTeachersByString() {
        return teachersByString;
    }

    public void setTeachersByString(String teachersByString) {
        this.teachersByString = teachersByString;
    }
}