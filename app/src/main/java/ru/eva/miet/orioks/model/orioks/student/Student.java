package ru.eva.miet.orioks.model.orioks.student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import ru.eva.miet.orioks.model.orioks.Institute;
import ru.eva.miet.orioks.model.orioks.UserGroup;
import ru.eva.miet.orioks.model.orioks.UserRole;

public class Student extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("group")
    @Expose
    private UserGroup group;
    @SerializedName("institute")
    @Expose
    private Institute institute;
    @SerializedName("role")
    @Expose
    private UserRole role;
    @SerializedName("course")
    @Expose
    private Integer course;
    @SerializedName("recordBookId")
    @Expose
    private String recordBookId;
    @SerializedName("semester")
    @Expose
    private String semester;
    @SerializedName("studyDirection")
    @Expose
    private String studyDirection;
    @SerializedName("studyProfile")
    @Expose
    private String studyProfile;
    @SerializedName("years")
    @Expose
    private String years;
    @SerializedName("error")
    @Expose
    private String error;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserGroup getGroup() {
        return group;
    }

    public void setGroup(UserGroup group) {
        this.group = group;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public String getRecordBookId() {
        return recordBookId;
    }

    public void setRecordBookId(String recordBookId) {
        this.recordBookId = recordBookId;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getStudyDirection() {
        return studyDirection;
    }

    public void setStudyDirection(String studyDirection) {
        this.studyDirection = studyDirection;
    }

    public String getStudyProfile() {
        return studyProfile;
    }

    public void setStudyProfile(String studyProfile) {
        this.studyProfile = studyProfile;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}