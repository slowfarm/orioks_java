package ru.eva.miet.orioks.model.orioks.student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Debt extends RealmObject {
    @SerializedName("consultationSchedule")
    @Expose
    private RealmList<String> consultationSchedule;
    @SerializedName("formControl")
    @Expose
    private String formControl;
    @SerializedName("currentScore")
    @Expose
    private Double currentScore;
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("institute")
    @Expose
    private String institute;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("maxScore")
    @Expose
    private Double maxScore;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("teachers")
    @Expose
    private RealmList<String> teachers = null;
    private int color;
    private double progress;
    private String rank;
    private String teachersByString;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getTeachersByString() {
        return teachersByString;
    }

    public void setTeachersByString(String teachersByString) {
        this.teachersByString = teachersByString;
    }


    public RealmList<String> getConsultationSchedule() {
        return consultationSchedule;
    }

    public void setConsultationSchedule(RealmList<String> consultationSchedule) {
        this.consultationSchedule = consultationSchedule;
    }

    public String getFormControl() {
        return formControl;
    }

    public void setFormControl(String formControl) {
        this.formControl = formControl;
    }

    public Double getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(Double currentScore) {
        this.currentScore = currentScore;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<String> getTeachers() {
        return teachers;
    }

    public void setTeachers(RealmList<String> teachers) {
        this.teachers = teachers;
    }

}
