package ru.eva.miet.orioks.adapter.student;

import android.graphics.PorterDuff;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnDisciplineItemClickListener;
import ru.eva.miet.orioks.model.orioks.student.Debt;

public class DebtFragmentAdapter extends RecyclerView.Adapter<DebtFragmentAdapter.ViewHolder> {

    private List<Debt> debtList;
    private OnDisciplineItemClickListener onDisciplineItemClickListener;

    public DebtFragmentAdapter(List<Debt> debtList) {
        this.debtList = debtList;
    }

    public void addItems(List<Debt> debtList) {
        this.debtList = debtList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.debt_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Debt debt = debtList.get(position);
        holder.debtTitleTextView.setText(debt.getName());
        holder.rankTextView.setText(debt.getRank());
        holder.typeTextView.setText(debt.getFormControl());
        holder.rankTextView.getBackground().setColorFilter(debt.getColor(), PorterDuff.Mode.DARKEN);
    }

    @Override
    public int getItemCount() {
        return debtList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView debtTitleTextView, rankTextView, typeTextView;

        ViewHolder(View itemView) {
            super(itemView);
            debtTitleTextView = itemView.findViewById(R.id.debt_title_text_view);
            rankTextView = itemView.findViewById(R.id.rank_text_view);
            typeTextView = itemView.findViewById(R.id.type_text_view);
            itemView.setOnClickListener(view-> onDisciplineItemClickListener.onClick(getAdapterPosition()));
        }
    }

    public void setOnDisciplineItemClickListener(OnDisciplineItemClickListener onDisciplineItemClickListener) {
        this.onDisciplineItemClickListener = onDisciplineItemClickListener;
    }
}