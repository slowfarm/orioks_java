package ru.eva.miet.orioks.fragment.student.discipline;

import android.view.View;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnDisciplinesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.SemesterList;
import ru.eva.miet.orioks.model.orioks.student.Discipline;


class PresenterDiscipline implements ContractDiscipline.Presenter,
        OnDisciplinesReceived,
        OnTokenReceived,
        OnSemestersReceived {
    private ContractDiscipline.View mView;
    private ContractDiscipline.Repository mRepository;
    private String error = "Несуществующий или аннулированный токен";

    PresenterDiscipline(ContractDiscipline.View mView) {
        this.mView = mView;
        mRepository = new RepositoryDiscipline();
    }

    @Override
    public void getDisciplineList() {
        mView.setRecyclerView(ConvertHelper.getInstance().convertDiscipline(mRepository.getDisciplineList()));
    }

    @Override
    public void setDisciplineList() {
        mRepository.setDisciplineList(this);
    }

    @Override
    public void onDisciplineClick(int position) {
        mView.startEventActivity(mRepository.getDisciplineId(position));
    }

    @Override
    public void getSemesters() {
        Semester semester = mRepository.getLocalSemester();
        if(semester.getList().size() != 0) {
            mView.setSemestersAdapter(ConvertHelper.getInstance().convertSemester(semester));
        } else {
            mView.hideSemesterFab();
        }
    }

    @Override
    public void onFabClick(int position) {
        List<SemesterList> semesterList = mRepository.getLocalSemester().getList();
        if (semesterList.get(position) != null) {
            mRepository.changeSemester(semesterList.get(position).getId(), this);
        }
    }

    @Override
    public void onResponse(List<Discipline> disciplineList) {
        mRepository.setDisciplineList(ConvertHelper.getInstance().convertDiscipline(disciplineList));
        mView.addRecyclerViewItems(disciplineList, disciplineList.size() == 0 ? View.VISIBLE : View.GONE);
        mView.unsetRefreshing();
    }

    @Override
    public void onResponse(AccessToken accessToken, int position) {
        finishApp();
    }

    @Override
    public void onResponse(Semester semester) {
        mRepository.clearDisciplines();
        mView.startSplashActivity();
    }

    @Override
    public void onFailure(String message) {
        mView.showToast(message);
        mView.unsetRefreshing();
        if(message.equals(error)) {
            mView.showToast("Токен аннулирован");
            mRepository.deleteToken(this);
        }
    }

    private void finishApp() {
        mRepository.clearAllTables();
        mView.finishActivity();
    }
}
