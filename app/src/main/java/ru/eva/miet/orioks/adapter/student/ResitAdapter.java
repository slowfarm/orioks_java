package ru.eva.miet.orioks.adapter.student;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.model.orioks.student.Resit;

public class ResitAdapter extends RecyclerView.Adapter<ResitAdapter.ViewHolder> {
    private List<Resit> resitList;

    public ResitAdapter(List<Resit> resitList) {
        this.resitList = resitList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resit, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Resit resit = resitList.get(position);
        holder.nameTextView.setText(resit.getResitNumber());
        holder.typeTextView.setText(resit.getDate());
        holder.classroomTextView.setText(resit.getClassroom());
    }

    @Override
    public int getItemCount() {
        return resitList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView, typeTextView, classroomTextView;

        ViewHolder(View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.name_text_view);
            typeTextView = itemView.findViewById(R.id.type_text_view);
            classroomTextView = itemView.findViewById(R.id.classroom_text_view);
        }
    }
}
