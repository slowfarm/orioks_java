package ru.eva.miet.orioks.model.orioks.teacher;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class TeacherEvent extends RealmObject {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("studyGroups")
    @Expose
    private RealmList<String> studyGroups = null;
    @SerializedName("disciplines")
    @Expose
    private RealmList<TeacherDiscipline> disciplines = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<String> getStudyGroups() {
        return studyGroups;
    }

    public void setStudyGroups(RealmList<String> studyGroups) {
        this.studyGroups = studyGroups;
    }

    public RealmList<TeacherDiscipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(RealmList<TeacherDiscipline> disciplines) {
        this.disciplines = disciplines;
    }

}
