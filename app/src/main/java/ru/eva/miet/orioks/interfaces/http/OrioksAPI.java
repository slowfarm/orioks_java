package ru.eva.miet.orioks.interfaces.http;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.ChangedRole;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.teacher.Journal;
import ru.eva.miet.orioks.model.orioks.student.Debt;
import ru.eva.miet.orioks.model.orioks.student.Discipline;
import ru.eva.miet.orioks.model.orioks.student.Event;
import ru.eva.miet.orioks.model.orioks.student.Resit;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.teacher.Score;
import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;
import ru.eva.miet.orioks.model.schedule.WeekType;

public interface OrioksAPI {

    @Headers({"Accept: application/json"})
    @POST("/api/v2/authorization")
    Call<AccessToken> getToken(@Header("Authorization") String loginPass,
                               @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/student/disciplines")
    Call<List<Discipline>> getDisciplines(@Header("Authorization") String token,
                                          @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/student/debts")
    Call<List<Debt>> getDebts(@Header("Authorization") String token,
                              @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/student")
    Call<Student> getStudent(@Header("Authorization") String token,
                             @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/teacher")
    Call<Student> getTeacher(@Header("Authorization") String token,
                             @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/student/disciplines/{id}/events")
    Call<List<Event>> getEvents(@Header("Authorization") String token,
                                @Header("User-Agent") String userAgent,
                                @Path("id") int id);

    @Headers({"Accept: application/json"})
    @DELETE("/api/v1/student/tokens/{token}")
    Call<AccessToken> deleteAccessToken(@Header("Authorization") String token,
                                        @Header("User-Agent") String userAgent,
                                        @Path("token") String token1);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/tokens")
    Call<List<Security>> getAllActiveTokens(@Header("Authorization") String token,
                                            @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("/api/v2/pushes/register")
    Call<String> registerPusher(@Header("Authorization") String token,
                                @Header("User-Agent") String userAgent,
                                @Body RequestBody body);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("/api/v2/pushes/unregister")
    Call<String> unregisterPusher(@Header("Authorization") String token,
                                  @Header("User-Agent") String userAgent,
                                  @Body RequestBody body);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/schedule")
    Call<WeekType> getWeekType(@Header("Authorization") String s,
                               @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @GET("/api/v1/student/academic-debts/{id}/resits")
    Call<List<Resit>> getResits(@Header("Authorization") String token,
                                @Header("User-Agent") String userAgent,
                                @Path("id") int id);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/teacher/disciplines")
    Call<List<TeacherEvent>> getTeacherEvents(@Header("Authorization") String token,
                                              @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/teacher/disciplines/{disciplineId}/groups/{groupId}")
    Call<Journal> getJournal(@Header("Authorization") String token,
                             @Header("User-Agent") String userAgent,
                             @Path("disciplineId") int disciplineId,
                             @Path("groupId") int groupId);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/teacher/disciplines/{disciplineId}/groups/{groupId}/students/{studentId}/events")
    Call<List<MeasureControl>> getMeasureControl(@Header("Authorization") String token,
                                                 @Header("User-Agent") String userAgent,
                                                 @Path("disciplineId") int disciplineId,
                                                 @Path("groupId") int groupId,
                                                 @Path("studentId") int studentId);

    @Headers({"Accept: application/json"})
    @PUT("/api/v2/teacher/disciplines/{disciplineId}/groups/{groupId}/students/{studentId}/events/{eventId}")
    Call<Score> setMeasureControlValue(@Header("Authorization") String token,
                                       @Header("User-Agent") String userAgent,
                                       @Path("disciplineId") int disciplineId,
                                       @Path("groupId") int groupId,
                                       @Path("studentId") int studentId,
                                       @Path("eventId") Integer eventId,
                                       @Body RequestBody body);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/roles")
    Call<Roles> getRoles(@Header("Authorization") String token,
                         @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @PATCH("/api/v2/roles/{roleName}")
    Call<ChangedRole> changeRole(@Header("Authorization") String token,
                                 @Header("User-Agent") String userAgent,
                                 @Path("roleName") String role);

    @Headers({"Accept: application/json"})
    @GET("/api/v2/semesters")
    Call<Semester> getSemester(@Header("Authorization") String token,
                               @Header("User-Agent") String userAgent);

    @Headers({"Accept: application/json"})
    @PATCH("/api/v2/semesters/{semesterId}")
    Call<Semester> changeSemester(@Header("Authorization") String token,
                                  @Header("User-Agent") String userAgent,
                                  @Path("semesterId") Integer semesterId);

//    @Headers({"Accept: application/json"})
//    @GET("/api/v1/schedule/timetable")
//    Call<Timing> getTimeTable(@Header("Authorization") String s, @Header("User-Agent") String userAgent);

    //    @Headers({"Accept: application/json"})
//    @GET("/api/v1/schedule/groups")
//    Call<List<Group>> getGroups(@Header("Authorization") String token, @Header("User-Agent") String userAgent);

//    @Headers({"Accept: application/json"})
//    @GET("/api/v1/schedule/groups/{id}")
//    Call<List<Schedule>> getSchedule(@Header("Authorization") String token, @Header("User-Agent") String userAgent, @Path("id") int id);
}