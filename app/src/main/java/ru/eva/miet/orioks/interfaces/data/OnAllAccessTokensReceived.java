package ru.eva.miet.orioks.interfaces.data;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.Security;

public interface OnAllAccessTokensReceived {
    void onResponse(List<Security> tokens);

    void onFailure(String message);
}
