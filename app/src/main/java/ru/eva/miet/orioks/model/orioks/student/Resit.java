package ru.eva.miet.orioks.model.orioks.student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Resit extends RealmObject {
    @SerializedName("classroom")
    @Expose
    private String classroom;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("resit_number")
    @Expose
    private String resitNumber;
    private Integer id;
    private String date;

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getResitNumber() {
        return resitNumber;
    }

    public void setResitNumber(String resitNumber) {
        this.resitNumber = resitNumber;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
