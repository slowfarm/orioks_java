package ru.eva.miet.orioks.fragment.teacher.event;

import android.widget.SearchView;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTeacherEventsReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.SemesterList;
import ru.eva.miet.orioks.model.orioks.teacher.StudentGroup;
import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;


class PresenterEvent implements ContractEvent.Presenter,
        OnTeacherEventsReceived,
        OnTokenReceived,
        OnSemestersReceived {
    private ContractEvent.View mView;
    private ContractEvent.Repository mRepository;
    private String error = "Несуществующий или аннулированный токен";

    PresenterEvent(ContractEvent.View mView) {
        this.mView = mView;
        mRepository = new RepositoryEvent();
    }

    @Override
    public void getDisciplineList() {
        mView.setRecyclerView(ConvertHelper.getInstance().convertTeacherDiscipline(mRepository.getLocalTeacherEventList()));
    }

    @Override
    public void setDisciplineList() {
        mRepository.getTeacherEventList(this);
    }

    @Override
    public void checkIsIndividual(int disciplineId, int groupId) {
        StudentGroup studyGroup = mRepository.getStudyGroup(groupId).getGroup();
        if(studyGroup == null) {
            mView.startJournalActivity(disciplineId, groupId);
        } else {
            mView.startMeasureControlActivity(disciplineId, groupId, studyGroup.getId());
        }
    }

    @Override
    public void onSearchViewClosed() {
        mView.onSearchViewClosed(ConvertHelper.getInstance().convertTeacherDiscipline(mRepository.getLocalTeacherEventList()));
    }

    @Override
    public void closeSearchView(SearchView searchView) {
        if(searchView != null && !searchView.isIconified()) {
            mView.closeSearchView();
        }
    }

    @Override
    public void getSemesters() {
        Semester semester = mRepository.getLocalSemester();
        if(semester.getList().size() != 0) {
            mView.setSemestersAdapter(ConvertHelper.getInstance().convertSemester(semester));
        } else {
            mView.hideSemesterFab();
        }
    }

    @Override
    public void onFabClick(int position) {
        List<SemesterList> semesterList = mRepository.getLocalSemester().getList();
        if (semesterList.get(position) != null) {
            mRepository.changeSemester(semesterList.get(position).getId(), this);
        }
    }

    @Override
    public void onResponse(List<TeacherEvent> teacherEventList) {
        if(teacherEventList != null) {
            mRepository.setTeacherEventList(teacherEventList);
            mView.addRecyclerViewItems(ConvertHelper.getInstance().convertTeacherDiscipline(teacherEventList));
        } else {
            mView.showToast("Произошла ошибка при обработке запроса");
        }
        mView.unsetRefreshing();
    }

    @Override
    public void onResponse(AccessToken accessToken, int position) {
        finishApp();
    }

    @Override
    public void onResponse(Semester semester) {
        mRepository.clearDisciplines();
        mView.startSplashActivity();
    }

    @Override
    public void onFailure(String message) {
        mView.showToast(message);
        mView.unsetRefreshing();
        if(message.equals(error)) {
            mView.showToast("Токен аннулирован");
            mRepository.deleteToken(this);
        }
    }

    private void finishApp() {
        mRepository.clearAllTables();
        mView.finishActivity();
    }
}
