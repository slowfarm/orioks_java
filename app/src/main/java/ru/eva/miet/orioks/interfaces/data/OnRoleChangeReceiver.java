package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.orioks.ChangedRole;

public interface OnRoleChangeReceiver {
    void onResponse(ChangedRole role);
    void onFailure(String message);
}
