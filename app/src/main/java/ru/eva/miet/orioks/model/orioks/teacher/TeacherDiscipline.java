package ru.eva.miet.orioks.model.orioks.teacher;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class TeacherDiscipline extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("formControl")
    @Expose
    private String formControl;
    @SerializedName("studyGroups")
    @Expose
    private RealmList<StudyGroup> studyGroups = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormControl() {
        return formControl;
    }

    public void setFormControl(String formControl) {
        this.formControl = formControl;
    }

    public List<StudyGroup> getStudyGroups() {
        return studyGroups;
    }

    public void setStudyGroups(RealmList<StudyGroup> studyGroups) {
        this.studyGroups = studyGroups;
    }

}