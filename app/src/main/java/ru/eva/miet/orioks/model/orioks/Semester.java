package ru.eva.miet.orioks.model.orioks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Semester extends RealmObject {

    @SerializedName("current")
    @Expose
    private Integer current;
    @SerializedName("list")
    @Expose
    private RealmList<SemesterList> list = null;
    @SerializedName("semester")
    @Expose
    private Integer semester;
    @SerializedName("error")
    @Expose
    private String error;

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public RealmList<SemesterList> getList() {
        return list;
    }

    public void setList(RealmList<SemesterList> list) {
        this.list = list;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
