package ru.eva.miet.orioks.activity.main;

import android.content.Context;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnStudentReceived;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.schedule.WeekType;

class RepositoryMain implements ContractMain.Repository {

    @Override
    public Student getLocalStudent() {
        return StorageHelper.getInstance().getStudent();
    }

    @Override
    public void getStudent(OnStudentReceived onStudentReceived) {
        NetworkHelper.getInstance().getStudent(StorageHelper.getInstance().getAccessToken(), onStudentReceived);
    }

    @Override
    public void getTeacher(OnStudentReceived onStudentReceived) {
        NetworkHelper.getInstance().getTeacher(StorageHelper.getInstance().getAccessToken(), onStudentReceived);
    }

    @Override
    public void setStudent(Student student) {
        StorageHelper.getInstance().setStudent(student);
    }

    @Override
    public WeekType getCurrentWeekType() {
        return StorageHelper.getInstance().getWeekType();
    }

    @Override
    public String getCurrentRole() {
        return StorageHelper.getInstance().getUserRoles();
    }

    @Override
    public void setIsNightThemeSelected(Context context, boolean isSelected) {
        StorageHelper.getInstance().setIsNightThemeSelected(context, isSelected);
    }

    @Override
    public boolean getIsDarkThemeSelected(Context context) {
        return StorageHelper.getInstance().getIsNightThemeSelected(context);
    }
}