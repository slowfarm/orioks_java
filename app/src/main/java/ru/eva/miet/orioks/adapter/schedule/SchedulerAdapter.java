package ru.eva.miet.orioks.adapter.schedule;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.model.schedule.Data;

public class SchedulerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Data> dataList;

    public SchedulerAdapter(List<Data> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        switch (i) {
            case 1:
                return new ViewHolder1(LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_separator, parent, false));
            default:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Data item = dataList.get(position);
        switch (viewHolder.getItemViewType()) {
            case 0:
                ViewHolder holder = (ViewHolder)viewHolder;
                holder.name.setText(item.getClazz().getName());
                holder.audience.setText(item.getRoom().getName());
                holder.teacher.setText(item.getClazz().getTeacher());
                holder.timeStart.setText(item.getTime().getTimeFrom());
                holder.timeEnd.setText(item.getTime().getTimeTo());

                break;
            default:
                ViewHolder1 holder1 = (ViewHolder1)viewHolder;
                if(dataList.size() != 1 && item.getDayOfWeek() != null) {
                    holder1.dayOfWeek.setText(item.getDayOfWeek());
                }
                else {
                    holder1.dayOfWeek.setText("Нет занятий");
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(dataList.get(position).getDay() != null)
            return 0;
        else
            return 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView name, audience, teacher, timeStart, timeEnd;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            audience = itemView.findViewById(R.id.audience);
            teacher = itemView.findViewById(R.id.teacher);
            timeStart = itemView.findViewById(R.id.time_start);
            timeEnd = itemView.findViewById(R.id.time_end);

//            itemView.setOnClickListener(view->
//                    DialogHelper
//                            .getInstance()
//                            .createDialog(dataList.get(getAdapterPosition()), view)
//                            .show());
        }
    }

    class ViewHolder1 extends RecyclerView.ViewHolder {
        private final TextView dayOfWeek;
        ViewHolder1(@NonNull View itemView) {
            super(itemView);
            dayOfWeek = itemView.findViewById(R.id.day_of_week);
        }
    }
}
