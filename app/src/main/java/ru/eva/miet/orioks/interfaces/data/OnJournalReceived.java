package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.orioks.teacher.Journal;

public interface OnJournalReceived {
    void onResponse(Journal journal, int groupId);
    void onFailure(String text);
}
