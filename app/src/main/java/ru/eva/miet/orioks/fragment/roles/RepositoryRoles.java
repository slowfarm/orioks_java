package ru.eva.miet.orioks.fragment.roles;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnRoleChangeReceiver;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.SemesterList;


class RepositoryRoles implements ContractRoles.Repository {



    @Override
    public void clearAllTables() {
        StorageHelper.getInstance().clearTables();
    }

    @Override
    public Roles getLocalRoles() {
        return StorageHelper.getInstance().getRoles();
    }

    @Override
    public void changeRole(String role, OnRoleChangeReceiver onRoleChangeReceiver) {
        NetworkHelper.getInstance().changeRole(StorageHelper.getInstance().getAccessToken(), role, onRoleChangeReceiver);
    }

    @Override
    public void getRoles(OnRolesReceived onRolesReceived) {
        NetworkHelper.getInstance().getRoles(StorageHelper.getInstance().getAccessToken(), onRolesReceived);
    }

    @Override
    public void setRoles(Roles roles) {
        StorageHelper.getInstance().setRoles(roles);
    }

    @Override
    public void clearDisciplines() {
        StorageHelper.getInstance().clearDisciplines();
    }
}
