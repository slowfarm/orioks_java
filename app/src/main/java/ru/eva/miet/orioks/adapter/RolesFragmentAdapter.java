package ru.eva.miet.orioks.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnRoleItemClickListener;
import ru.eva.miet.orioks.model.orioks.RolesList;

public class RolesFragmentAdapter extends RecyclerView.Adapter<RolesFragmentAdapter.ViewHolder> {

    private OnRoleItemClickListener onRoleItemClickListener;
    private List<RolesList> rolesList;

    public RolesFragmentAdapter(List<RolesList> rolesList) {
        this.rolesList = rolesList;
    }

    public void setOnRoleItemClickListener(OnRoleItemClickListener onRoleItemClickListener) {
        this.onRoleItemClickListener = onRoleItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_role, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RolesList role = rolesList.get(position);
        holder.roleTextView.setText(role.getDescription());
        holder.isCurrentTextView.setVisibility(role.isCurrent() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return rolesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView roleTextView, isCurrentTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            roleTextView = itemView.findViewById(R.id.role_text_view);
            isCurrentTextView = itemView.findViewById(R.id.is_current_text_view);
            itemView.setOnClickListener(v->onRoleItemClickListener.onClick(rolesList.get(getAdapterPosition()).getRole()));
        }
    }
}
