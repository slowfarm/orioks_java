package ru.eva.miet.orioks.fragment.schedulepager.tomorrow;

import java.util.List;

import ru.eva.miet.orioks.model.schedule.Data;
import ru.eva.miet.orioks.model.schedule.WeekType;


class ContractTomorrowFragment {

    interface View {

        void setAdapter(List<Data> schedule);
    }

    interface Presenter {

        void getSchedule(String group);
    }

    interface Repository {

        List<Data>  getSchedule(int week, int day, String group);

        WeekType getCurrentWeekType();
    }
}
