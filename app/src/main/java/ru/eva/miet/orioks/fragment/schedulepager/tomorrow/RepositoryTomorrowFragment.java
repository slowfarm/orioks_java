package ru.eva.miet.orioks.fragment.schedulepager.tomorrow;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.model.schedule.Data;
import ru.eva.miet.orioks.model.schedule.WeekType;


class RepositoryTomorrowFragment implements ContractTomorrowFragment.Repository {
    @Override
    public List<Data> getSchedule(int week, int day, String group) {
        return StorageHelper.getInstance().getSchedulersDataCurrentDay(week, day, group);
    }

    @Override
    public WeekType getCurrentWeekType() {
        return StorageHelper.getInstance().getWeekType();
    }
}
