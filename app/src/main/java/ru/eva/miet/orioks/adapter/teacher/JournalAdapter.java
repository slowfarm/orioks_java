package ru.eva.miet.orioks.adapter.teacher;

import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnJournalItemClickListener;
import ru.eva.miet.orioks.model.orioks.teacher.JournalStudent;

public class JournalAdapter extends RecyclerView.Adapter<JournalAdapter.ViewHolder> {

    private List<JournalStudent> studentList;
    private OnJournalItemClickListener onJournalItemClickListener;

    public JournalAdapter(List<JournalStudent> studentList) {
        this.studentList = studentList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.discipline_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        JournalStudent student = studentList.get(position);
        holder.disciplineTitleTextView.setText(student.getFullName());
        holder.rankTextView.setText(student.getCurrentScoreInString());
        holder.typeTextView.setText(student.getCurrentGradInString());
        holder.rankTextView.getBackground().setColorFilter(student.getColor(), PorterDuff.Mode.DARKEN);
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public void setOnJournalItemClickListener(OnJournalItemClickListener onJournalItemClickListener) {
        this.onJournalItemClickListener = onJournalItemClickListener;
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView disciplineTitleTextView, rankTextView, typeTextView;

        ViewHolder(View itemView) {
            super(itemView);
            disciplineTitleTextView = itemView.findViewById(R.id.discipline_title_text_view);
            rankTextView = itemView.findViewById(R.id.rank_text_view);
            typeTextView = itemView.findViewById(R.id.type_text_view);
            itemView.setOnClickListener(view-> onJournalItemClickListener.onClick(studentList.get(getAdapterPosition()).getId()));
        }
    }
}
