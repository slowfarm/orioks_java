package ru.eva.miet.orioks.activity.schedule;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnScheduleReceived;
import ru.eva.miet.orioks.model.schedule.Schedule;

class PresenterSchedulerActivity implements ContractSchedulerActivity.Presenter, OnScheduleReceived {
    private ContractSchedulerActivity.View mView;
    private ContractSchedulerActivity.Repository mRepository;


    PresenterSchedulerActivity(ContractSchedulerActivity.View mView) {
        this.mView = mView;
        mRepository = new RepositorySchedulerActivity();
    }

    @Override
    public void setPagerAdapter(String group) {
        if(mRepository.getLocalSchedule(group) == null) {
            mRepository.getSchedule(this, group);
        } else {
            mView.setPagerAdapter();
        }
    }

    @Override
    public void setViewPagerToCurrentWeek() {
        int currentWeek = ConvertHelper.getInstance().getCurrentWeek(mRepository.getCurrentWeekType().getSemesterStart());
        mView.setViewPagerToPosition(currentWeek + 2);
    }

    @Override
    public void onResponse(Schedule schedule) {
        if(schedule != null) {
            mRepository.setSchedule(schedule);
            mView.setPagerAdapter();
        }
    }

    @Override
    public void onFailure(String message) {
        mView.showToast("Нет соединения с интернетом");
    }

}
