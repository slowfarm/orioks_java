package ru.eva.miet.orioks.interfaces.view;

public interface OnDisciplineItemClickListener {
    void onClick(int position);
}
