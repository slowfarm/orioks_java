package ru.eva.miet.orioks.fragment.teacher.event;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.auth.AuthActivity;
import ru.eva.miet.orioks.activity.splash.SplashActivity;
import ru.eva.miet.orioks.activity.teacher.journal.JournalActivity;
import ru.eva.miet.orioks.activity.teacher.measure.MeasureControlActivity;
import ru.eva.miet.orioks.adapter.teacher.TeacherEventFragmentAdapter;
import ru.eva.miet.orioks.interfaces.view.OnEventItemClickListener;
import ru.eva.miet.orioks.model.ParentDiscipline;
import ru.eva.miet.orioks.model.orioks.SemesterList;

public class EventFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        ContractEvent.View,
        OnEventItemClickListener {

    private TeacherEventFragmentAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ContractEvent.Presenter mPresenter;
    private RecyclerView recyclerView;

    private View view;
    private SearchView searchView;

    private RapidFloatingActionLayout fabLayout;
    private RapidFloatingActionButton fab;
    private RapidFloatingActionHelper helper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_event, container, false);
        setHasOptionsMenu(true);
        Resources resources = getResources();
        int orange = resources.getColor(R.color.grade_orange);
        int yellow = resources.getColor(R.color.grade_amber);
        int lightGreen = resources.getColor(R.color.grade_lime);
        int green = resources.getColor(R.color.grade_green);

        fabLayout = view.findViewById(R.id.fab_layout);
        fab = view.findViewById(R.id.fab);

        swipeRefreshLayout = view.findViewById(R.id.container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(orange, yellow, lightGreen, green);

        mPresenter = new PresenterEvent(this);
        mPresenter.getDisciplineList();
        mPresenter.setDisciplineList();

        mPresenter.getSemesters();
        return view;
    }

    @Override
    public void onRefresh() {
        mPresenter.setDisciplineList();
    }

    @Override
    public void setRecyclerView(List<ParentDiscipline> eventList) {
        recyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        adapter = new TeacherEventFragmentAdapter(eventList);
        adapter.setOnItemClickListener(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void addRecyclerViewItems(List<ParentDiscipline> eventList) {
        adapter.addItems(eventList);
        mPresenter.closeSearchView(searchView);
    }

    @Override
    public void setSemestersAdapter(List<RFACLabelItem> localSemester) {
        RapidFloatingActionContentLabelList rfaContent = new RapidFloatingActionContentLabelList(view.getContext());
        rfaContent.setItems(localSemester);
        rfaContent.setOnRapidFloatingActionContentLabelListListener(
                new RapidFloatingActionContentLabelList.OnRapidFloatingActionContentLabelListListener() {
                    @Override
                    public void onRFACItemLabelClick(int position, RFACLabelItem item) {
                        helper.toggleContent();
                        mPresenter.onFabClick(position);
                    }
                    @Override
                    public void onRFACItemIconClick(int position, RFACLabelItem item) { }
                });
        helper = new RapidFloatingActionHelper(view.getContext(), fabLayout, fab, rfaContent).build();
    }

    @Override
    public void startSplashActivity() {
        ((Activity)view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), SplashActivity.class));
    }

    @Override
    public void hideSemesterFab() {
        fab.setVisibility(View.GONE);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(view.getContext(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void unsetRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void finishActivity() {
        ((Activity) view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), AuthActivity.class));
    }

    @Override
    public void startJournalActivity(int disciplineId, int groupId) {
        view.getContext().startActivity(
                new Intent(view.getContext(), JournalActivity.class)
                        .putExtra("disciplineId", disciplineId)
                        .putExtra("groupId", groupId));
    }

    @Override
    public void startMeasureControlActivity(int disciplineId, int groupId, int studentId) {
        view.getContext().startActivity(
                new Intent(view.getContext(), MeasureControlActivity.class)
                .putExtra("disciplineId", disciplineId)
                .putExtra("groupId", studentId)
                .putExtra("studentId", groupId));
    }

    @Override
    public void onClick(int disciplineId, int groupId) {
        mPresenter.checkIsIndividual(disciplineId, groupId);
    }

    @Override
    public void scrollToBottom() {
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.event_fragment_teacher, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchItem.getActionView();
        searchView.setOnCloseListener(() -> {
            mPresenter.onSearchViewClosed();
            return false;
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if(adapter != null) {
                    adapter.filterData(newText);
                }
                return true;
            }
        });
    }

    @Override
    public void onSearchViewClosed(List<ParentDiscipline> eventList) {
        adapter.addItems(eventList);
    }

    @Override
    public void closeSearchView() {
        searchView.setQuery("", true);
        searchView.setIconified(true);
    }
}