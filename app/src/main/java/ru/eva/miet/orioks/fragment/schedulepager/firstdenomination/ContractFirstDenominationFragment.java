package ru.eva.miet.orioks.fragment.schedulepager.firstdenomination;

import java.util.List;

import ru.eva.miet.orioks.model.schedule.Data;


class ContractFirstDenominationFragment {

    interface View {

        void setAdapter(List<Data> schedule);
    }

    interface Presenter {

        void getSchedule(String group);
    }

    interface Repository {

        List<Data> getSchedule(int week, String group);
    }
}
