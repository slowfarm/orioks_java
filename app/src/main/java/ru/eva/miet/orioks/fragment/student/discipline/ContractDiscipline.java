package ru.eva.miet.orioks.fragment.student.discipline;

import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnDisciplinesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.student.Discipline;


class ContractDiscipline {
    interface View {

        void setRecyclerView(List<Discipline> disciplineList);

        void addRecyclerViewItems(List<Discipline> disciplineList, int visibility);

        void showToast(String text);

        void unsetRefreshing();

        void finishActivity();

        void startEventActivity(int id);

        void setSemestersAdapter(List<RFACLabelItem> convertSemester);

        void startSplashActivity();

        void hideSemesterFab();
    }

    interface Presenter {

        void getDisciplineList();

        void setDisciplineList();

        void onDisciplineClick(int position);

        void getSemesters();

        void onFabClick(int position);
    }

    interface Repository {

        List<Discipline> getDisciplineList();

        void setDisciplineList(OnDisciplinesReceived onDisciplinesReceived);

        void setDisciplineList(List<Discipline> disciplineList);

        void deleteToken(OnTokenReceived onTokenReceived);

        void clearAllTables();

        int getDisciplineId(int position);

        Semester getLocalSemester();

        void changeSemester(Integer id, OnSemestersReceived onSemestersReceived);

        void clearDisciplines();
    }
}
