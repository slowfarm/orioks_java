package ru.eva.miet.orioks.helper.domain;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import ru.eva.miet.orioks.model.LocalGroup;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.RolesList;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.SemesterList;
import ru.eva.miet.orioks.model.orioks.student.Debt;
import ru.eva.miet.orioks.model.orioks.student.Discipline;
import ru.eva.miet.orioks.model.orioks.student.Event;
import ru.eva.miet.orioks.model.orioks.student.Resit;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.orioks.teacher.Journal;
import ru.eva.miet.orioks.model.orioks.teacher.JournalStudent;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.teacher.StudyGroup;
import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;
import ru.eva.miet.orioks.model.schedule.Data;
import ru.eva.miet.orioks.model.schedule.Schedule;
import ru.eva.miet.orioks.model.schedule.WeekType;

public class StorageHelper {
    private static volatile StorageHelper instance;
    private String table = "table";

    public static StorageHelper getInstance() {
        if (instance == null)
            instance = new StorageHelper();
        return instance;
    }

    public void setLogin(String login, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(table, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("login", login);
        editor.apply();
    }

    public String getLogin(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(table, Context.MODE_PRIVATE);
        return sharedPref.getString("login", null);
    }

    public void setFirebaseToken(String token, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(table, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("token", token);
        editor.apply();
    }

    public String getFirebaseToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(table, Context.MODE_PRIVATE);
        return sharedPref.getString("token", null);
    }

    public void setAccessToken(AccessToken accessToken) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(AccessToken.class);
            realm.insert(accessToken);
        });
    }

    public String getAccessToken() {
        Realm realm = Realm.getDefaultInstance();
        AccessToken accessToken = realm.where(AccessToken.class).findFirst();
        if (accessToken != null) {
            return realm.copyFromRealm(accessToken).getToken();
        } else {
            return null;
        }
    }

    public void setStudent(Student student) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Student.class);
            realm.insert(student);
        });
    }

    public Student getStudent() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Student.class).findFirst();
    }

    public void setDisciplines(List<Discipline> disciplines) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Discipline.class);
            realm.insert(disciplines);
        });
    }

    public List<Discipline> getDisciplines() {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(Discipline.class).findAll());
    }

    public Discipline getDiscipline(int id) {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(Discipline.class).equalTo("id", id).findFirst());
    }

    public List<Event> getEventsList(int id) {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(Event.class).sort("week", Sort.ASCENDING).equalTo("id", id).findAll());
    }

    public void setEvents(List<Event> eventList, int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.where(Event.class).equalTo("id", id).findAll().deleteAllFromRealm();
            realm.insert(eventList);
        });
    }

    public void clearTables() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm1.deleteAll());
    }

    public List<Security> getAllActiveTokens() {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(Security.class).sort("lastUsed", Sort.DESCENDING).findAll());
    }

    public void setAllActiveTokens(List<Security> tokens) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Security.class);
            realm.insert(tokens);
        });
        realm.close();
    }

    public void deleteActiveToken(AccessToken token) {
        Realm realm = Realm.getDefaultInstance();
        if (token != null && token.getToken() != null)
            realm.executeTransaction(realm1 -> {
                Security realmToken = realm1.where(Security.class).equalTo("token", token.getToken()).findFirst();
                realmToken.deleteFromRealm();
            });
    }

//    public List<Group> getGroups() {
//        Realm realm = Realm.getDefaultInstance();
//        return realm.copyFromRealm(realm.where(Group.class).findAll());
//    }
//
//    public void removeGroup(Group group) {
//        Realm realm = Realm.getDefaultInstance();
//        if (group != null)
//            realm.executeTransaction(realm1 -> {
//                Group realmGroup = realm1.where(Group.class).equalTo("id", group.getIdGroup()).findFirst();
//                realmGroup.deleteFromRealm();
//            });
//    }
//
//    public void addGroup(Group group) {
//        Realm realm = Realm.getDefaultInstance();
//        realm.executeTransaction(realm1 -> realm1.insert(group));
//    }
//
//    public List<Schedule> getSchedule(int id) {
//        Realm realm = Realm.getDefaultInstance();
//        Group group = realm.where(Group.class).equalTo("id", id).findFirst();
//        if (group != null && group.getSchedule().size() > 0)
//            return realm.copyFromRealm(group.getSchedule());
//        else return null;
//    }

//    public List<Schedule> getScheduleCurrentDay(int week, int day, int id) {
//        Realm realm = Realm.getDefaultInstance();
//        Group group = realm.where(Group.class).equalTo("id", id).findFirst();
//        if (group != null) {
//            List<Schedule> scheduleList = new ArrayList<>();
//            for (Schedule schedule : group.getSchedule()) {
//                if(schedule.getWeek() == week && schedule.getDay() == day)
//                    scheduleList.add(schedule);
//            }
//            return scheduleList;
//        } else return null;
//    }

//    public List<Schedule> getScheduleCurrentWeek(int week, int id) {
//        Realm realm = Realm.getDefaultInstance();
//        Group group = realm.where(Group.class).equalTo("id", id).findFirst();
//        if (group != null) {
//            List<Schedule> scheduleList = new ArrayList<>();
//            for (Schedule schedule : group.getSchedule()) {
//                if(schedule.getWeek() == week)
//                    scheduleList.add(schedule);
//            }
//            return scheduleList;
//        } else return null;
//    }

    public void setSchedule(Schedule schedule) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Schedule.class);
            realm.delete(Data.class);
            realm.insert(schedule);
        });
        realm.close();
    }

//    public int getGroupId(String group) {
//        Realm realm = Realm.getDefaultInstance();
//        Group realmGroup = realm.where(Group.class).equalTo("name", group).findFirst();
//        if(realmGroup != null)
//        return realmGroup.getIdGroup();
//        else return 0;
//    }

//    public void setTimingTable(Timing timingTable) {
//        Realm realm = Realm.getDefaultInstance();
//        realm.executeTransaction(realm1 -> {
//            realm.delete(Timing.class);
//            realm.insert(timingTable);
//        });
//    }

    public void setTypeOfWeek(WeekType typeOfWeek) {
        if (typeOfWeek != null) {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> {
                realm.delete(WeekType.class);
                realm.insert(typeOfWeek);
            });
        }
    }

    public WeekType getWeekType() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(WeekType.class).findFirst();
    }


    public void addLocalGroup(String group) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            LocalGroup localGroup = realm1.where(LocalGroup.class).findFirst();
            if (localGroup != null) {
                localGroup.getGroup().add(group);
            } else {
                localGroup = new LocalGroup();
                RealmList<String> groupList = new RealmList<>();
                groupList.add(group);
                localGroup.setGroup(groupList);
                realm1.insert(localGroup);
            }
        });
        realm.close();
    }

    public void removeGroup(String group) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            LocalGroup localGroup = realm1.where(LocalGroup.class).findFirst();
            if (localGroup != null) {
                for (int i = 0; i < localGroup.getGroup().size(); i++) {
                    if (localGroup.getGroup().get(i).equals(group)) {
                        localGroup.getGroup().remove(i);
                        break;
                    }
                }
            }
        });
        realm.close();
    }

    public List<String> getGroups() {
        Realm realm = Realm.getDefaultInstance();
        LocalGroup groups = realm.where(LocalGroup.class).findFirst();
        if (groups != null) {
            groups = realm.copyFromRealm(groups);
            realm.close();
            return groups.getGroup();
        }
        realm.close();
        return new ArrayList<>();
    }

    public Schedule getSchedule(String group) {
        Realm realm = Realm.getDefaultInstance();
        Schedule scheduler = realm.where(Schedule.class).equalTo("data.group.name", group).findFirst();
        if (scheduler != null) {
            scheduler = realm.copyFromRealm(scheduler);
        }
        realm.close();
        return scheduler;
    }

    public List<Data> getSchedulersDataCurrentWeek(int dayNumber, String group) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Data> results = realm.where(Data.class)
                .equalTo("dayNumber", dayNumber)
                .contains("group.name", group)
                .findAll()
                .sort("time.timeFrom");
        RealmResults<Data> results1 = results.sort("day");
        List<Data> dataList = realm.copyFromRealm(results1);
        realm.close();
        return dataList;
    }

    public List<Data> getSchedulersDataCurrentDay(int week, int value, String group) {
        Realm realm = Realm.getDefaultInstance();
        List<Data> dataList = realm.copyFromRealm(
                realm.where(Data.class)
                        .equalTo("dayNumber", week)
                        .equalTo("day", value)
                        .contains("group.name", group)
                        .sort("time.timeFrom")
                        .findAll());
        realm.close();
        return dataList;
    }

    public WeekType getTypeOfWeek() {
        WeekType weekType;
        Realm realm = Realm.getDefaultInstance();
        weekType = realm.where(WeekType.class).findFirst();
        realm.close();
        return weekType;
    }

    public void deleteLocalToken() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> realm.delete(AccessToken.class));
        realm.close();
    }

    public List<Debt> getDebts() {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(Debt.class).findAll());
    }

    public void setDebts(List<Debt> debts) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Debt.class);
            realm.insert(debts);
        });
        realm.close();
    }

    public Debt getDebt(int id) {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(Debt.class).equalTo("id", id).findFirst());
    }

    public void setResits(List<Resit> resitList, int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.where(Resit.class).equalTo("id", id).findAll().deleteAllFromRealm();
            realm.insert(resitList);
        });
        realm.close();
    }

    public List<Resit> getResitList(int id) {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(Resit.class).equalTo("id", id).findAll());
    }

    public String getUserRoles() {
        Realm realm = Realm.getDefaultInstance();
        Roles roles = realm.where(Roles.class).findFirst();
        if (roles == null)
            return null;
        else
            roles = realm.copyFromRealm(roles);
        realm.close();
        return roles.getCurrent();
    }

    public List<TeacherEvent> getTeacherEventList() {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(TeacherEvent.class).findAll());
    }

    public void setTeacherEventList(List<TeacherEvent> teacherEventList) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(TeacherEvent.class);
            realm.insert(teacherEventList);
        });
        realm.close();
    }

    public String getGroup(int groupId) {
        String groupName = "";
        Realm realm = Realm.getDefaultInstance();
        StudyGroup studyGroup = realm.where(StudyGroup.class).equalTo("id", groupId).findFirst();
        if (studyGroup != null) {
            studyGroup = realm.copyFromRealm(studyGroup);
            groupName = studyGroup.getFullName();
        }
        realm.close();
        return groupName;
    }

    public String getStudentName(int studentId) {
        String studentName = "";
        Realm realm = Realm.getDefaultInstance();
        JournalStudent student = realm.where(JournalStudent.class).equalTo("id", studentId).findFirst();
        if (student != null) {
            student = realm.copyFromRealm(student);
            studentName = student.getFullName();
        }
        realm.close();
        return studentName;
    }

    public void setJournal(Journal journal) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.where(Journal.class).equalTo("id", journal.getId()).findAll().deleteAllFromRealm();
            realm.insert(journal);
        });
        realm.close();
    }

    public List<JournalStudent> getJournal(int groupId) {
        Realm realm = Realm.getDefaultInstance();
        List<JournalStudent> students = realm.where(JournalStudent.class).equalTo("groupId", groupId).findAll();
        if(students != null)
            students = realm.copyFromRealm(students);
        else
            students = new ArrayList<>();
        realm.close();
        return students;
    }

    public String getDebtorStudentName(int studentId) {
        String studentName = "";
        Realm realm = Realm.getDefaultInstance();
        StudyGroup student = realm.where(StudyGroup.class).equalTo("id", studentId).findFirst();
        if (student != null) {
            student = realm.copyFromRealm(student);
            studentName = student.getFullName();
        }
        realm.close();
        return studentName;
    }

    public StudyGroup getStudyGroup(int groupId) {
        Realm realm = Realm.getDefaultInstance();
        StudyGroup student = realm.where(StudyGroup.class).equalTo("id", groupId).findFirst();
        return student;
    }

    public void setIsNightThemeSelected(Context context, boolean isSelected) {
        SharedPreferences sharedPref = context.getSharedPreferences(table, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("nightMode", isSelected);
        editor.apply();
    }

    public boolean getIsNightThemeSelected(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(table, Context.MODE_PRIVATE);
        return sharedPref.getBoolean("nightMode", false);
    }

    public void setMeasureControl(List<MeasureControl> measureControlList,int disciplineId, int studentId) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.where(MeasureControl.class).equalTo("studentId", studentId).equalTo("disciplineId", disciplineId).findAll().deleteAllFromRealm();
            realm.insert(measureControlList);
        });
        realm.close();
    }

    public List<MeasureControl> getMeasureControl(int disciplineId, int studentId) {
        Realm realm = Realm.getDefaultInstance();
        List<MeasureControl> measureControlList = realm.copyFromRealm(
                realm.where(MeasureControl.class).equalTo("studentId", studentId).equalTo("disciplineId", disciplineId).findAll());
        realm.close();
        return  measureControlList;
    }

    public void setRoles(Roles roles) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Roles.class);
            realm.delete(RolesList.class);
            realm.insert(roles);
        });
        realm.close();
    }

    public Roles getRoles() {
        Realm realm = Realm.getDefaultInstance();
        Roles roles;
        roles = realm.where(Roles.class).findFirst();
        if(roles != null) {
            roles = realm.copyFromRealm(roles);
        }
        realm.close();
        return  roles;
    }

    public Semester getSemester() {
        Realm realm = Realm.getDefaultInstance();
        Semester semester;
        semester = realm.where(Semester.class).findFirst();
        if(semester != null) {
            semester = realm.copyFromRealm(semester);
        }
        realm.close();
        return semester;
    }

    public void setSemester(Semester semester) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Semester.class);
            realm.delete(SemesterList.class);
            realm.insert(semester);
        });
        realm.close();
    }

    public void clearDisciplines() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm.delete(Discipline.class);
        });
        realm.close();
    }
}
