package ru.eva.miet.orioks.activity.teacher.measure;

import android.widget.EditText;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnMeasureControlReceived;
import ru.eva.miet.orioks.interfaces.data.OnMeasureControlUpdated;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;

class ContractMeasureControl {
    interface View {

        void setToolbarTitle(String toolbarTitle);

        void showToast(String text);

        void clearListView();

        void addWeek(Integer week,
                     List<MeasureControl> measureControlList);

        void addEvents(MeasureControl measureControl);
    }

    interface Presenter {

        void setToolbarTitle(int studentId);

        void getMeasureControl(int disciplineId,
                               int groupId,
                               int studentId);

        void addMeasureControl(Integer week,
                               List<MeasureControl> measureControlList);

        void checkValue(EditText editText,
                        MeasureControl measureControl,
                        int disciplineId,
                        int groupId,
                        int studentId);
    }

    interface Repository {
        String getStudentName(int studentId);

        void getMeasureControl(int disciplineId,
                               int groupId,
                               int studentId,
                               OnMeasureControlReceived onMeasureControlReceived);

        void setMeasureControlValue(RequestBody requestBody,
                                    int disciplineId,
                                    int groupId,
                                    int studentId,
                                    Integer eventId,
                                    OnMeasureControlUpdated onMeasureControlUpdated);

        String getDebtorStudentName(int studentId);

        void setMeasureControl(List<MeasureControl> measureControlList, int disciplineId, int id);

        List<MeasureControl> getLocalMeasureControl(int disciplineId, int studentId);
    }
}
