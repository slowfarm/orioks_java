package ru.eva.miet.orioks.fragment.student.debt;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.auth.AuthActivity;
import ru.eva.miet.orioks.activity.student.resit.ResitActivity;
import ru.eva.miet.orioks.adapter.student.DebtFragmentAdapter;
import ru.eva.miet.orioks.interfaces.view.OnDisciplineItemClickListener;
import ru.eva.miet.orioks.model.orioks.student.Debt;

public class DebtFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        ContractDebt.View,
        OnDisciplineItemClickListener {

    private DebtFragmentAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ContractDebt.Presenter mPresenter;

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_debt, container, false);

        Resources resources = getResources();
        int orange = resources.getColor(R.color.grade_orange);
        int yellow = resources.getColor(R.color.grade_amber);
        int lightGreen = resources.getColor(R.color.grade_lime);
        int green = resources.getColor(R.color.grade_green);

        swipeRefreshLayout = view.findViewById(R.id.container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(orange, yellow, lightGreen, green);

        mPresenter = new PresenterDebt(this);
        mPresenter.getDebtList();
        mPresenter.setDebtList();

        return view;
    }

    @Override
    public void onRefresh() {
        mPresenter.setDebtList();
    }

    @Override
    public void setRecyclerView(List<Debt> debtList) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        adapter = new DebtFragmentAdapter(debtList);
        adapter.setOnDisciplineItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void addRecyclerVIewItems(List<Debt> debtList, int visibility) {
        adapter.addItems(debtList);
        view.findViewById(R.id.no_debt_text_view).setVisibility(visibility);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(view.getContext(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void unsetRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void finishActivity() {
        ((Activity)view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), AuthActivity.class));
    }

    @Override
    public void startEventActivity(int id) {
        view.getContext().startActivity(new Intent(view.getContext(), ResitActivity.class).putExtra("id", id));
    }

    @Override
    public void onClick(int position) {
        mPresenter.onDebtClick(position);
    }
}