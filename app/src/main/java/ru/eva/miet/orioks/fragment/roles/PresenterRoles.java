package ru.eva.miet.orioks.fragment.roles;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnRoleChangeReceiver;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.model.orioks.ChangedRole;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.Semester;


class PresenterRoles implements ContractRoles.Presenter,
        OnRolesReceived,
        OnRoleChangeReceiver,
        OnSemestersReceived {
    private ContractRoles.View mView;
    private ContractRoles.Repository mRepository;

    PresenterRoles(ContractRoles.View mView) {
        this.mView = mView;
        mRepository = new RepositoryRoles();
    }

    @Override
    public void getLocalRoles() {
        mView.setAdapter(ConvertHelper.getInstance().convertRoles(mRepository.getLocalRoles()));
    }

    @Override
    public void changeRole(String role) {
        mRepository.changeRole(role, this);
    }

    @Override
    public void refreshRoles() {
        mRepository.getRoles(this);
    }

    @Override
    public void onResponse(ChangedRole role) {
        mRepository.clearDisciplines();
        mView.startSplashActivity();
    }

    @Override
    public void onResponse(Roles roles) {
        mView.unsetRefreshing();
        if (roles != null) {
            mRepository.setRoles(roles);
            mView.setAdapter(ConvertHelper.getInstance().convertRoles(roles));
        } else {
            mView.showToast("Токен анулирован");
            finishApp();
        }
    }

    @Override
    public void onResponse(Semester semester) {
        mView.startSplashActivity();
    }

    @Override
    public void onFailure(String message) {
        mView.showToast("Нет соединения с интернетом");
        mView.unsetRefreshing();
    }


    private void finishApp() {
        mRepository.clearAllTables();
        mView.finishActivity();
    }
}
