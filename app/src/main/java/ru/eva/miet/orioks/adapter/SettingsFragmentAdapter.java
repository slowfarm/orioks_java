package ru.eva.miet.orioks.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnItemViewClickListener;
import ru.eva.miet.orioks.model.orioks.Security;


public class SettingsFragmentAdapter extends RecyclerView.Adapter<SettingsFragmentAdapter.ViewHolder> {
    private List<Security> tokenList;
    private OnItemViewClickListener onItemViewClickListener;

    public SettingsFragmentAdapter(List<Security> tokenList) {
        this.tokenList = tokenList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_settings, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingsFragmentAdapter.ViewHolder holder, int position) {
        Security token = tokenList.get(position);
        holder.userAgentTextView.setText(token.getUserAgent());
        holder.lastUsedTextView.setText(token.isCurrent() ? "Текущая сессия" : token.getDate());
    }

    @Override
    public int getItemCount() {
        return tokenList.size();
    }

    public void removeItem(int position) {
        tokenList.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView lastUsedTextView, userAgentTextView;

        ViewHolder(View itemView) {
            super(itemView);
            lastUsedTextView = itemView.findViewById(R.id.last_used_text_view);
            userAgentTextView = itemView.findViewById(R.id.user_agent_text_view);
            itemView.setOnClickListener(v -> onItemViewClickListener.onClick(tokenList.get(getAdapterPosition()), getAdapterPosition(), v));
        }
    }

    public void setOnItemViewClickListener(OnItemViewClickListener onItemViewClickListener) {
        this.onItemViewClickListener = onItemViewClickListener;
    }






}