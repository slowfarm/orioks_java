package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.schedule.Schedule;

public interface OnScheduleReceived {

    void onResponse(Schedule scheduler);
    void onFailure(String message);
}
