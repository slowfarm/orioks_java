package ru.eva.miet.orioks.activity.student.resit;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.Toast;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.adapter.student.ResitAdapter;
import ru.eva.miet.orioks.model.orioks.student.Debt;
import ru.eva.miet.orioks.model.orioks.student.Resit;

public class ResitActivity extends AppCompatActivity implements ContractResit.View {

    private ContractResit.Presenter mPresenter;
    private AppCompatTextView disciplineTitleTextView;
    private ResitAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resit);

        Intent intent = getIntent();
        String intExtraName = "id";
        int id = intent.getIntExtra(intExtraName, 0);
        disciplineTitleTextView = findViewById(R.id.debt_title_text_view);
        mPresenter = new PresenterResit(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        mPresenter.setToolbarTitle(id);

        mPresenter.setHeaderData(id);
        mPresenter.getResits(id);
    }

    @Override
    public void setToolbarTitle(String toolbarTitle) {
        disciplineTitleTextView.setText(toolbarTitle);
    }

    @Override
    public void setHeaderData(Debt debt) {
        ((AppCompatTextView)findViewById(R.id.type_text_view)).setText(debt.getFormControl());
        ((AppCompatTextView)findViewById(R.id.teachers_text_view)).setText(debt.getTeachersByString());
        ((AppCompatTextView)findViewById(R.id.department_text_view)).setText(debt.getInstitute());
    }

    @Override
    public void setRecyclerView(List<Resit> resitList) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        adapter = new ResitAdapter(resitList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
