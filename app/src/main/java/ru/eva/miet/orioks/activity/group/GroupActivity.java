package ru.eva.miet.orioks.activity.group;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.adapter.schedule.GroupAdapter;
import ru.eva.miet.orioks.interfaces.view.OnGroupItemClickListener;


public class GroupActivity extends AppCompatActivity implements ContractGroupActivity.View,
        OnGroupItemClickListener {

    private ContractGroupActivity.Presenter mPresenter;
    private GroupAdapter adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Группы");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mPresenter = new PresenterGroupActivity(this);
        mPresenter.getGroups();
    }


    @SuppressLint("CheckResult")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gruop, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if(adapter != null) {
                    adapter.filterData(newText);
                }
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setRecyclerView(List<String> groupList) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        adapter = new GroupAdapter(groupList);
        adapter.setOnGroupItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(String group) {
        setResult(RESULT_OK, new Intent().putExtra("group", group));
        finish();
    }

    @Override
    public void onShortcutAddClick(String group) {
    }

    @Override
    public void onLongClick(int position, String group) {
    }
}
