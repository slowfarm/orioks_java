package ru.eva.miet.orioks.interfaces.view;

public interface OnJournalItemClickListener {
    void onClick(int id);
}
