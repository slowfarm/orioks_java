package ru.eva.miet.orioks.adapter.schedule;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnGroupItemClickListener;


public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {

    private List<String> groupList;
    private OnGroupItemClickListener onGroupItemClickListener;
    private List<String> filteredData;

    public GroupAdapter(List<String> groupList) {
        this.groupList = groupList;
        this.filteredData = groupList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.group_picker_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.group.setText(filteredData.get(position));
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    public void filterData(String constraint) {
        filteredData = mapData(constraint, groupList);
        notifyDataSetChanged();
    }

    private List<String> mapData(String constraint,  List<String> list) {
        List<String> nlist = new ArrayList<>();
        for (String item : list)
            if (item.toLowerCase().contains(constraint.toLowerCase()))
                nlist.add(item);
        return nlist;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView group;

        ViewHolder(View itemView) {
            super(itemView);
            group = itemView.findViewById(R.id.group_text_view);
            itemView.setOnClickListener(v -> onGroupItemClickListener.onClick(filteredData.get(getAdapterPosition())));
        }
    }

    public void setOnGroupItemClickListener(OnGroupItemClickListener onGroupItemClickListener) {
        this.onGroupItemClickListener = onGroupItemClickListener;
    }
}