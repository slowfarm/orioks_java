package ru.eva.miet.orioks.interfaces.data;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.student.Debt;

public interface OnDebtsReceived {
    void onResponse(List<Debt> disciplineList);

    void onFailure(String message);
}
