package ru.eva.miet.orioks.helper;

import android.graphics.Color;

import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.model.ChildDiscipline;
import ru.eva.miet.orioks.model.ParentDiscipline;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.SemesterList;
import ru.eva.miet.orioks.model.orioks.student.Debt;
import ru.eva.miet.orioks.model.orioks.student.Discipline;
import ru.eva.miet.orioks.model.orioks.student.Event;
import ru.eva.miet.orioks.model.orioks.student.Resit;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.teacher.JournalStudent;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.RolesList;
import ru.eva.miet.orioks.model.orioks.teacher.StudyGroup;
import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;
import ru.eva.miet.orioks.model.schedule.Data;

public class ConvertHelper {
    private static volatile ConvertHelper instance;

    public static ConvertHelper getInstance() {
        if (instance == null)
            instance = new ConvertHelper();
        return instance;
    }

    public List<Discipline> convertDiscipline(List<Discipline> disciplines) {
        for (Discipline discipline : disciplines) {
            double progress = 100;
            discipline.setRank(String.valueOf(discipline.getScore()));
            if (discipline.getScore() - discipline.getScore().intValue() == 0)
                discipline.setRank(String.valueOf(discipline.getScore().intValue()));
            if (discipline.getCurrentMaxScore() != 0) {
                progress = discipline.getScore() / discipline.getCurrentMaxScore() * 100;
            } else {
                discipline.setRank("-");
            }
            discipline.setColor(getColor(progress));
            if (discipline.getRank().equals("-"))
                discipline.setColor(Color.TRANSPARENT);
            discipline.setTeachersByString(teachersToString(discipline.getTeachers()));
        }
        return disciplines;
    }

    public List<Debt> convertDebt(List<Debt> debts) {
        for (Debt debt : debts) {
            double progress = 100;
            debt.setRank(String.valueOf(debt.getCurrentScore()));
            if (debt.getCurrentScore() - debt.getCurrentScore().intValue() == 0)
                debt.setRank(String.valueOf(debt.getCurrentScore().intValue()));
            if (debt.getMaxScore() != 0) {
                progress = debt.getCurrentScore() / debt.getMaxScore() * 100;
            } else {
                debt.setRank("-");
            }
            debt.setColor(getColor(progress));
            if (debt.getRank().equals("-"))
                debt.setColor(Color.TRANSPARENT);
            debt.setTeachersByString(teachersToString(debt.getTeachers()));
        }
        return debts;
    }

    private String teachersToString(List<String> teachers) {
        StringBuilder teacherList = new StringBuilder();
        if (teachers != null) {
            for (String teacher : teachers) {
                teacherList.append(teacher).append(", ");
            }
            if (teacherList.length() > 0) {
                teacherList = new StringBuilder(teacherList.substring(0, teacherList.length() - 2));
                return teacherList.toString();
            } else
                return "Не назначен";
        } else
            return "Не назначен";
    }

    private int getColor(double progress) {
        if (progress < 0) {
            return Color.parseColor("#039BE5");
        } else if (progress < 50) {
            return Color.parseColor("#E53935");
        } else if (progress < 70) {
            return Color.parseColor("#FF9800");
        } else if (progress < 85) {
            return Color.parseColor("#CDDC39");
        } else {
            return Color.parseColor("#66BB6A");
        }
    }

    public List<Security> convertTokens(List<Security> tokens, String currentToken) {
        for (Security token : tokens) {
            token.setDate(dateParser(token.getLastUsed()));
            if (token.getToken().equals(currentToken))
                token.setCurrent(true);
        }
        return tokens;
    }

    private String dateParser(String inputDate) {
        String oldPattern = "yyyy-MM-dd HH:mm:ss";
        String newPattern = "HH:mm dd.MM.yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(oldPattern, Locale.getDefault());
        Date date = new Date();
        Date dateNow = new Date();
        try {
            date = sdf.parse(inputDate);
            sdf.applyPattern(newPattern);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int hour = 1000 * 60 * 60;
        int day = hour * 24;
        int week = day * 7;
        long diff = dateNow.getTime() - date.getTime();
        if (diff < hour)
            return "Недавно";
        if (diff < day) {
            if (TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) == 1)
                return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " час назад";
            return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) + " часа(ов) назад";
        }
        if (diff < week) {
            if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) == 1)
                return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + " день назад";
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + " дня(ей) назад";
        }
        return sdf.format(date);
    }

    public Event convertEvent(Event event) {
        if (event.getCurrentScore() != null) {
            event.setCurrentGradeInString(String.valueOf(event.getCurrentScore()));
            if (event.getCurrentScore() - event.getCurrentScore().intValue() == 0)
                event.setCurrentGradeInString(String.valueOf(event.getCurrentScore().intValue()));
            if (event.getCurrentScore() == -1)
                event.setCurrentGradeInString("н");
            event.setColor(getColor(event.getCurrentScore() / event.getMaxScore() * 100));
        } else {
            event.setCurrentGradeInString("-");
            event.setColor(getColor(-1));
        }
        event.setMaxGradeInString("Из " + event.getMaxScore().intValue());
        return event;
    }

    public MeasureControl convertMeasureControl(MeasureControl measureControl) {
        if (measureControl.getCurrentScore() != null) {
            measureControl.setCurrentGradeInString(String.valueOf(measureControl.getCurrentScore()));
            if (measureControl.getCurrentScore() - measureControl.getCurrentScore().intValue() == 0)
                measureControl.setCurrentGradeInString(String.valueOf(measureControl.getCurrentScore().intValue()));
            if (measureControl.getCurrentScore() == -1)
                measureControl.setCurrentGradeInString("н");
            measureControl.setColor(getColor(measureControl.getCurrentScore() / measureControl.getMaxScore() * 100));
        } else {
            measureControl.setCurrentGradeInString("-");
            measureControl.setColor(getColor(-1));
        }
        measureControl.setMaxGradeInString("Из " + measureControl.getMaxScore().intValue());
        return measureControl;
    }

    public int getCurrentWeek(String semesterStart) {
        int week = getCurrentWeekIndicator(semesterStart);
        return week % 4;
    }

    public int getCurrentWeekIndicator(String semesterStart) {
        String format = "yyyy-MM-dd";

        SimpleDateFormat df = new SimpleDateFormat(format, Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        Date date = null;
        try {
            date = df.parse(semesterStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cal.setTime(date);
        int startWeek = cal.get(Calendar.WEEK_OF_YEAR);
        cal.setTime(new Date());
        int currentWeek = cal.get(Calendar.WEEK_OF_YEAR);
        if ((currentWeek - startWeek) < 0 || (currentWeek - startWeek) > 17)
            return 17;
        return currentWeek - startWeek;
    }

    public String getCurrentValue(int week) {
        String value = "1 Числитель";
        switch (week % 4) {
            case 0:
                value = "1 Числитель";
                break;
            case 1:
                value = "1 Знаменатель";
                break;
            case 2:
                value = "2 Числитель";
                break;
            case 3:
                value = "2 Знаменатель";
                break;
        }
        return value;
    }

    public int getCurrentDayOfWeek() {
        Calendar c = Calendar.getInstance(Locale.getDefault());
        int currentDay = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (currentDay == 0)
            return 7;
        return currentDay;
    }

    public int getNextDayOfWeek() {
        int nextDayOfWeek = getCurrentDayOfWeek() + 1;
        if (nextDayOfWeek == 8)
            return 1;
        else return nextDayOfWeek;
    }

    public List<Data> scheduleWeek(List<Data> schedule) {
        if (schedule == null)
            schedule = new ArrayList<>();
        else {
            fillDataList(schedule);
        }
        return schedule;
    }

    private void fillDataList(List<Data> dataList) {
        for (int i = 1; i < dataList.size(); i++) {
            if (!dataList.get(i).getDay().equals(dataList.get(i - 1).getDay())) {
                setDataToPosition(dataList, i);
                i++;
            }
        }
        setDataToPosition(dataList, 0);
    }

    private void setDataToPosition(List<Data> dataList, int position) {
        Data data = new Data();
        data.setDayOfWeek(getDayOfWeek(dataList.get(position).getDay()));
        dataList.add(position, data);
    }

    public List<Data> scheduleDay(List<Data> schedule) {
        if (schedule == null) {
            schedule = new ArrayList<>();
        } else if (schedule.size() > 0) {
            setDataToPosition(schedule, 0);
        } else {
            schedule.add(0, new Data());
        }
        return schedule;
    }

    private String getDayOfWeek(int day) {
        switch (day) {
            case 2:
                return "Вторник";
            case 3:
                return "Среда";
            case 4:
                return "Четверг";
            case 5:
                return "Пятница";
            case 6:
                return "Суббота";
            case 7:
                return "Воскресенье";
            default:
                return "Понедельник";
        }
    }

    public String convertError(ResponseBody string) {
        String error = "Ошибка сервера";
        try {
            error = new JSONObject(string.string()).getString("error");
        } catch (Exception e) {
            try {
                error = new JSONObject(string.string()).getString("message");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return error;
    }

    public void convertResit(List<Resit> resits, int id) {
        for (Resit resit : resits) {
            resit.setId(id);
            resit.setDate(resitDateParser(resit.getDatetime()));
        }
    }

    private String resitDateParser(String inputDate) {
        String oldPattern = "dd-MM-yyyy'T'HH:mm";
        String newPattern = "HH:mm dd.MM.yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(oldPattern, Locale.getDefault());
        Date date = new Date();
        try {
            date = sdf.parse(inputDate);
            sdf.applyPattern(newPattern);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }

    public List<ParentDiscipline> convertTeacherDiscipline(List<TeacherEvent> localTeacherEventList) {
        List<ParentDiscipline> parentDisciplines = new ArrayList<>();
        for (TeacherEvent event : localTeacherEventList) {
            ParentDiscipline parentDiscipline = new ParentDiscipline();
            parentDiscipline.setName(event.getName());
            List<ChildDiscipline> childDisciplines = new ArrayList<>();
            for (int i = 0; i < event.getDisciplines().size(); i++) {
                for (StudyGroup studyGroup : event.getDisciplines().get(i).getStudyGroups()) {
                    ChildDiscipline childDiscipline = new ChildDiscipline();
                    if(studyGroup.getGroup() != null) {
                        childDiscipline.setName(studyGroup.getFullName() +", "+ studyGroup.getGroup().getFullName());
                    } else {
                        childDiscipline.setName(studyGroup.getFullName());
                    }
                    childDiscipline.setIdGroup(studyGroup.getId());
                    childDiscipline.setIdDiscipline(event.getDisciplines().get(i).getId());
                    childDisciplines.add(childDiscipline);
                }
            }
            parentDiscipline.setChildDisciplines(childDisciplines);
            parentDisciplines.add(parentDiscipline);
        }
        return parentDisciplines;
    }

    public List<JournalStudent> ConvertJournalStudents(List<JournalStudent> students) {
        for (JournalStudent student : students) {
            student.setCurrentScoreInString(String.valueOf(student.getCurrentScore()));
            student.setCurrentGradInString("Текущая оценка: " + student.getCurrentGrade());
            student.setColor(getJournalColor(student.getCurrentGrade()));
        }
        return students;
    }
    private int getJournalColor(Integer currentGrade) {
        switch (currentGrade) {
            case 3:
                return Color.parseColor("#FF9800");
            case 4:
                return Color.parseColor("#CDDC39");
            case 5:
                return Color.parseColor("#66BB6A");
            default:
                return Color.parseColor("#E53935");
        }
    }

    public List<RolesList> convertRoles(Roles roles) {
        List<RolesList>  rolesList = roles.getList();
        for(RolesList role : rolesList) {
            if(roles.getCurrent().equals(role.getRole())) {
                role.setCurrent(true);
                break;
            }
        }
        return  rolesList;
    }

    public List<RFACLabelItem> convertSemester(Semester semesters) {
     List<RFACLabelItem> items = new ArrayList<>();
        for(SemesterList semester : semesters.getList()) {
            if(semesters.getCurrent().equals(semester.getId())){
                items.add(new RFACLabelItem<Integer>().setLabel(semester.getName()).setResId(R.drawable.ic_check));
            } else {
                items.add(new RFACLabelItem<Integer>().setLabel(semester.getName()));
            }
        }
        return items;
    }
}
