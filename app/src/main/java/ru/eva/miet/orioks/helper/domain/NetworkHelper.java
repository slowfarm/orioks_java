package ru.eva.miet.orioks.helper.domain;


import androidx.annotation.NonNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.eva.miet.orioks.App;
import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnDebtsReceived;
import ru.eva.miet.orioks.interfaces.data.OnDisciplinesReceived;
import ru.eva.miet.orioks.interfaces.data.OnEventsReceived;
import ru.eva.miet.orioks.interfaces.data.OnGroupsReceived;
import ru.eva.miet.orioks.interfaces.data.OnJournalReceived;
import ru.eva.miet.orioks.interfaces.data.OnMeasureControlReceived;
import ru.eva.miet.orioks.interfaces.data.OnMeasureControlUpdated;
import ru.eva.miet.orioks.interfaces.data.OnPusherReceived;
import ru.eva.miet.orioks.interfaces.data.OnResitsReceived;
import ru.eva.miet.orioks.interfaces.data.OnRoleChangeReceiver;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnScheduleReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnStudentReceived;
import ru.eva.miet.orioks.interfaces.data.OnTeacherEventsReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.interfaces.data.OnWeekTypeReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.ChangedRole;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.teacher.Journal;
import ru.eva.miet.orioks.model.orioks.student.Debt;
import ru.eva.miet.orioks.model.orioks.student.Discipline;
import ru.eva.miet.orioks.model.orioks.student.Event;
import ru.eva.miet.orioks.model.orioks.student.Resit;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.teacher.Score;
import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;
import ru.eva.miet.orioks.model.schedule.Schedule;
import ru.eva.miet.orioks.model.schedule.WeekType;


public class NetworkHelper {
    private static NetworkHelper instance;

    public static NetworkHelper getInstance() {
        if (instance == null)
            instance = new NetworkHelper();
        return instance;
    }

    public void getAccessToken(String encodedString, OnTokenReceived onTokenReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getToken(encodedString, userAgent).enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                if (response.errorBody() == null)
                    onTokenReceived.onResponse(response.body(), -1);
                else {
                    onTokenReceived.onFailure(ConvertHelper.getInstance().convertError(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                onTokenReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getDisciplines(String token, OnDisciplinesReceived onDisciplinesReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getDisciplines("Bearer " + token, userAgent).enqueue(new Callback<List<Discipline>>() {
            @Override
            public void onResponse(Call<List<Discipline>> call, Response<List<Discipline>> response) {
                if (response.errorBody() == null) {
                    onDisciplinesReceived.onResponse(response.body());
                } else {
                    onDisciplinesReceived.onFailure(ConvertHelper.getInstance().convertError(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<List<Discipline>> call, Throwable t) {
                onDisciplinesReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getDebts(String token, OnDebtsReceived onDebtsReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getDebts("Bearer " + token, userAgent).enqueue(new Callback<List<Debt>>() {
            @Override
            public void onResponse(Call<List<Debt>> call, Response<List<Debt>> response) {
                onDebtsReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<Debt>> call, Throwable t) {
                onDebtsReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getStudent(String token, OnStudentReceived onStudentReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getStudent("Bearer " + token, userAgent).enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                if (response.errorBody() == null)
                    onStudentReceived.onResponse(response.body());
                else {
                    onStudentReceived.onFailure(ConvertHelper.getInstance().convertError(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {
                onStudentReceived.onFailure(t.getMessage());
            }
        });
    }


    public void getTeacher(String token, OnStudentReceived onStudentReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getTeacher("Bearer " + token, userAgent).enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                if (response.errorBody() == null)
                    onStudentReceived.onResponse(response.body());
                else {
                    onStudentReceived.onFailure(ConvertHelper.getInstance().convertError(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {
                onStudentReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getEvents(String token, int id, OnEventsReceived onEventsReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getEvents("Bearer " + token, userAgent, id).enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                List<Event> eventList = response.body();
                onEventsReceived.onResponse(eventList, id);
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                onEventsReceived.onFailure(t.getMessage(), id);
            }
        });
    }


    public void deleteAccessToken(String token, int position, OnTokenReceived onTokenReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().deleteAccessToken("Bearer " + token, userAgent, token).enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                onTokenReceived.onResponse(response.body(), position);
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                onTokenReceived.onFailure(t.getMessage());
            }
        });
    }


    public void getAllActiveTokens(String token, OnAllAccessTokensReceived onAllAccessTokensReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getAllActiveTokens("Bearer " + token, userAgent).enqueue(new Callback<List<Security>>() {
            @Override
            public void onResponse(Call<List<Security>> call, Response<List<Security>> response) {
                onAllAccessTokensReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<Security>> call, Throwable t) {
                onAllAccessTokensReceived.onFailure(t.getMessage());
            }
        });
    }

    public void registerPusher(String token, RequestBody requestBody, OnPusherReceived onPusherReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().registerPusher("Bearer " + token, userAgent, requestBody).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                onPusherReceived.onResponse();
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {
                onPusherReceived.onFailure();
            }
        });
    }

    public void unregisterPusher(String token, RequestBody requestBody, OnPusherReceived onPusherReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().unregisterPusher("Bearer " + token, userAgent, requestBody).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                onPusherReceived.onResponse();
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {
                onPusherReceived.onFailure();
            }
        });
    }

    public void getGroups(OnGroupsReceived onGroupsReceived) {
        App.getScheduleAPI().getGroups().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(@NonNull Call<String[]> call, @NonNull Response<String[]> response) {
                onGroupsReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<String[]> call, @NonNull Throwable t) {
                onGroupsReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getSchedule(String group, OnScheduleReceived onScheduleReceived) {
        App.getScheduleAPI().getScheduler(group).enqueue(new Callback<Schedule>() {
            @Override
            public void onResponse(Call<Schedule> call, Response<Schedule> response) {
                onScheduleReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<Schedule> call, Throwable t) {
                onScheduleReceived.onFailure(t.getMessage());
            }
        });

    }

//    public void getTimeTable(String token) {
//        String userAgent = "Orioks/1.0 Android "+android.os.Build.VERSION.RELEASE;
//        App.getApi().getTimeTable("Bearer "+token, userAgent).enqueue(new Callback<Timing>() {
//            @Override
//            public void onResponse(Call<Timing> call, Response<Timing> response) {
//                onTimeTableReceived.onResponse(response.body());
//            }
//
//            @Override
//            public void onFailure(Call<Timing> call, Throwable t) {
//                onTimeTableReceived.onFailure(t);
//            }
//        });
//    }

    public void getWeekType(String token, OnWeekTypeReceived onWeekTypeReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getWeekType("Bearer " + token, userAgent).enqueue(new Callback<WeekType>() {
            @Override
            public void onResponse(Call<WeekType> call, Response<WeekType> response) {
                onWeekTypeReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<WeekType> call, Throwable t) {
                onWeekTypeReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getResits(String token, int id, OnResitsReceived onResitsReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getResits("Bearer " + token, userAgent, id).enqueue(new Callback<List<Resit>>() {
            @Override
            public void onResponse(Call<List<Resit>> call, Response<List<Resit>> response) {
                onResitsReceived.onResponse(response.body(), id);
            }

            @Override
            public void onFailure(Call<List<Resit>> call, Throwable t) {
                onResitsReceived.onFailure(t.getMessage(), id);
            }
        });
    }

    public void getTeacherEvents(String token, OnTeacherEventsReceived onTeacherEventsReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getTeacherEvents("Bearer " + token, userAgent).enqueue(new Callback<List<TeacherEvent>>() {
            @Override
            public void onResponse(Call<List<TeacherEvent>> call, Response<List<TeacherEvent>> response) {
                onTeacherEventsReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<TeacherEvent>> call, Throwable t) {
                onTeacherEventsReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getJournal(String token, int disciplineId, int groupId, OnJournalReceived onJournalReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getJournal("Bearer " + token, userAgent, disciplineId, groupId).enqueue(new Callback<Journal>() {
            @Override
            public void onResponse(Call<Journal> call, Response<Journal> response) {
                onJournalReceived.onResponse(response.body(), groupId);
            }

            @Override
            public void onFailure(Call<Journal> call, Throwable t) {
                onJournalReceived.onFailure(t.getMessage());
            }
        });
    }

    public void getMeasureControl(String token, int disciplineId, int groupId, int studentId, OnMeasureControlReceived onMeasureControlReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getMeasureControl("Bearer " + token, userAgent, disciplineId, groupId, studentId).enqueue(new Callback<List<MeasureControl>>() {
            @Override
            public void onResponse(Call<List<MeasureControl>> call, Response<List<MeasureControl>> response) {
                onMeasureControlReceived.onResponse(response.body(), disciplineId, studentId);
            }

            @Override
            public void onFailure(Call<List<MeasureControl>> call, Throwable t) {
                onMeasureControlReceived.onFailure(t.getMessage());
            }
        });
    }

    public void setMeasureControlValue(String token, RequestBody requestBody, int disciplineId, int groupId, int studentId, Integer eventId, OnMeasureControlUpdated onMeasureControlUpdated) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().setMeasureControlValue("Bearer " + token, userAgent, disciplineId, groupId, studentId, eventId, requestBody).enqueue(new Callback<Score>() {
            @Override
            public void onResponse(Call<Score> call, Response<Score> response) {
                if (response.body().getError() == null) {
                    onMeasureControlUpdated.onResponse(disciplineId, groupId, studentId);
                } else {
                    onMeasureControlUpdated.onFailure(response.body().getError());
                }
            }

            @Override
            public void onFailure(Call<Score> call, Throwable t) {
                onMeasureControlUpdated.onFailure(t.getMessage());
            }
        });
    }

    public void getRoles(String token, OnRolesReceived onRolesReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getRoles("Bearer " + token, userAgent).enqueue(new Callback<Roles>() {
            @Override
            public void onResponse(Call<Roles> call, Response<Roles> response) {
                onRolesReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<Roles> call, Throwable t) {
                onRolesReceived.onFailure(t.getMessage());
            }
        });
    }

    public void changeRole(String token, String role, OnRoleChangeReceiver onRoleChangeReceiver) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().changeRole("Bearer " + token, userAgent, role).enqueue(new Callback<ChangedRole>() {
            @Override
            public void onResponse(Call<ChangedRole> call, Response<ChangedRole> response) {
                onRoleChangeReceiver.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<ChangedRole> call, Throwable t) {
                onRoleChangeReceiver.onFailure(t.getMessage());
            }
        });
    }

    public void getSemesters(String token, OnSemestersReceived onSemestersReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().getSemester("Bearer " + token, userAgent).enqueue(new Callback<Semester>() {
            @Override
            public void onResponse(Call<Semester> call, Response<Semester> response) {
                onSemestersReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<Semester> call, Throwable t) {
                onSemestersReceived.onFailure(t.getMessage());
            }
        });
    }

    public void changeSemester(String token, Integer semesterId, OnSemestersReceived onSemestersReceived) {
        String userAgent = "Orioks/1.0 Android " + android.os.Build.VERSION.RELEASE;
        App.getApi().changeSemester("Bearer "+token, userAgent, semesterId).enqueue(new Callback<Semester>() {
            @Override
            public void onResponse(Call<Semester> call, Response<Semester> response) {
                onSemestersReceived.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<Semester> call, Throwable t) {
                onSemestersReceived.onFailure(t.getMessage());
            }
        });
    }
}
