package ru.eva.miet.orioks.activity.teacher.journal;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.teacher.measure.MeasureControlActivity;
import ru.eva.miet.orioks.adapter.teacher.JournalAdapter;
import ru.eva.miet.orioks.interfaces.view.OnJournalItemClickListener;
import ru.eva.miet.orioks.model.orioks.teacher.JournalStudent;

public class JournalActivity extends AppCompatActivity implements ContractJournal.View, OnJournalItemClickListener {

    Toolbar toolbar;
    private int disciplineId;
    private int groupId;
    private ContractJournal.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal);
        mPresenter = new PresenterJournal(this);
        disciplineId = getIntent().getIntExtra("disciplineId", 0);
        groupId = getIntent().getIntExtra("groupId", 0);

        toolbar = findViewById(R.id.toolbar);
        mPresenter.setToolbarTitle(groupId);

        mPresenter.getEvents(disciplineId, groupId);
    }

    @Override
    public void setToolbarTitle(String  toolbarTitle) {
        toolbar.setTitle(toolbarTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setAdapter(List<JournalStudent> students) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        JournalAdapter adapter = new JournalAdapter(students);
        adapter.setOnJournalItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(int id) {
        startActivity(new Intent(this, MeasureControlActivity.class)
        .putExtra("disciplineId", disciplineId)
        .putExtra("groupId", groupId)
        .putExtra("studentId", id));
    }
}
