package ru.eva.miet.orioks.fragment.schedule;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.group.GroupActivity;
import ru.eva.miet.orioks.activity.schedule.SchedulerActivity;
import ru.eva.miet.orioks.adapter.schedule.SchedulerFragmentAdapter;
import ru.eva.miet.orioks.interfaces.view.OnGroupItemClickListener;


public class ScheduleFragment extends Fragment implements ContractSchedule.View,
        OnGroupItemClickListener {


    private ContractSchedule.Presenter mPresenter;
    private SchedulerFragmentAdapter adapter;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_schedule, container, false);


        FloatingActionButton add = view.findViewById(R.id.add_btn);
        add.setImageResource(R.drawable.ic_add);
        add.setOnClickListener(v-> startActivityForResult(new Intent(view.getContext(), GroupActivity.class),1));

        mPresenter = new PresenterSchedule(this);
        mPresenter.getRecyclerView();
        return view;
    }

    @Override
    public void setRecyclerView(List<String> groupList, int visibility) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        adapter = new SchedulerFragmentAdapter(groupList);
        adapter.setOnGroupItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(adapter);
        view.findViewById(R.id.no_schedule_text_view).setVisibility(visibility);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == -1) {
            mPresenter.addLocalGroup(data.getStringExtra("group"));
            view.findViewById(R.id.no_schedule_text_view).setVisibility(View.GONE);
        }
    }

    @Override
    public void notifyDataSetChanged(List<String> groupList) {
        adapter.addItems(groupList);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(view.getContext(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(String group) {
        startActivity(new Intent(view.getContext(), SchedulerActivity.class).putExtra("group", group));
    }

    @Override
    public void onShortcutAddClick(String group) {
        mPresenter.addPinnedShortcut(view.getContext(), group);
    }

    @Override
    public void onLongClick(int position, String group) {
        mPresenter.removeGroup(group);
        adapter.removeItem(position);
    }
}