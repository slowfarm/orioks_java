package ru.eva.miet.orioks.fragment.teacher.event;

import android.widget.SearchView;

import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTeacherEventsReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.ParentDiscipline;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.SemesterList;
import ru.eva.miet.orioks.model.orioks.teacher.StudyGroup;
import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;


class ContractEvent {
    interface View {

        void setRecyclerView(List<ParentDiscipline> eventList);

        void addRecyclerViewItems(List<ParentDiscipline> eventList);

        void showToast(String text);

        void unsetRefreshing();

        void finishActivity();

        void startJournalActivity(int disciplineId, int groupId);

        void startMeasureControlActivity(int disciplineId, int groupId, int studentId);

        void onSearchViewClosed(List<ParentDiscipline> convertTeacherDiscipline);

        void closeSearchView();

        void setSemestersAdapter(List<RFACLabelItem> convertSemester);

        void startSplashActivity();

        void hideSemesterFab();
    }

    interface Presenter {

        void getDisciplineList();

        void setDisciplineList();

        void checkIsIndividual(int disciplineId, int groupId);

        void onSearchViewClosed();

        void closeSearchView(SearchView searchView);

        void getSemesters();

        void onFabClick(int position);
    }

    interface Repository {

        void deleteToken(OnTokenReceived onTokenReceived);

        void clearAllTables();

        List<TeacherEvent> getLocalTeacherEventList();

        void getTeacherEventList(OnTeacherEventsReceived onTeacherEventsReceived);

        void setTeacherEventList(List<TeacherEvent> eventList);

        StudyGroup getStudyGroup(int groupId);

        Semester getLocalSemester();

        void changeSemester(Integer id, OnSemestersReceived onSemestersReceived);

        void clearDisciplines();
    }
}
