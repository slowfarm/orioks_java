package ru.eva.miet.orioks.model.orioks;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Security extends RealmObject {

    @SerializedName("lastUsed")
    private String lastUsed;
    @SerializedName("token")
    private String token;
    @SerializedName("userAgent")
    private String userAgent;
    private String date;
    private boolean isCurrent = false;

    public String getToken() {
        return token;
    }

    public String getLastUsed() {
        return lastUsed;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }
}
