package ru.eva.miet.orioks.activity.teacher.measure;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnMeasureControlReceived;
import ru.eva.miet.orioks.interfaces.data.OnMeasureControlUpdated;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;

class RepositoryMeasureControl implements ContractMeasureControl.Repository {
    @Override
    public String getStudentName(int studentId) {
        return StorageHelper.getInstance().getStudentName(studentId);
    }

    @Override
    public void getMeasureControl(int disciplineId, int groupId, int studentId, OnMeasureControlReceived onMeasureControlReceived) {
        NetworkHelper.getInstance().getMeasureControl(
                StorageHelper.getInstance().getAccessToken(),
                disciplineId,
                groupId,
                studentId,
                onMeasureControlReceived);
    }

    @Override
    public void setMeasureControlValue(RequestBody requestBody,
                                       int disciplineId,
                                       int groupId,
                                       int studentId,
                                       Integer eventId,
                                       OnMeasureControlUpdated onMeasureControlUpdated) {
        NetworkHelper.getInstance().setMeasureControlValue(StorageHelper.getInstance().getAccessToken(),
                requestBody,
                disciplineId,
                groupId,
                studentId,
                eventId,
                onMeasureControlUpdated);
    }

    @Override
    public String getDebtorStudentName(int studentId) {
        return StorageHelper.getInstance().getDebtorStudentName(studentId);
    }

    @Override
    public void setMeasureControl(List<MeasureControl> measureControlList, int disciplineId, int studentId) {
        StorageHelper.getInstance().setMeasureControl(measureControlList, disciplineId, studentId);
    }

    @Override
    public List<MeasureControl> getLocalMeasureControl(int disciplineId, int studentId) {
        return StorageHelper.getInstance().getMeasureControl(disciplineId, studentId);
    }
}
