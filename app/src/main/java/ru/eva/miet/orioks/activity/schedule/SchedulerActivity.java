package ru.eva.miet.orioks.activity.schedule;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.adapter.schedule.SchedulerFragmentPagerAdapter;


public class SchedulerActivity extends AppCompatActivity implements ContractSchedulerActivity.View {

    private ViewPager viewPager;

    private ContractSchedulerActivity.Presenter mPresenter;
    private SchedulerFragmentPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduler);
        String group = getIntent().getStringExtra("group");

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(group);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = findViewById(R.id.view_pager);
        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        adapter = new SchedulerFragmentPagerAdapter(getSupportFragmentManager());
        adapter.setGroup(group);

        mPresenter = new PresenterSchedulerActivity(this);
        mPresenter.setPagerAdapter(group);
    }

    @Override
    public void setPagerAdapter() {
        viewPager.setAdapter(adapter);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setViewPagerToPosition(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_date) {
            mPresenter.setViewPagerToCurrentWeek();
            return true;
        }
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.schedule, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
