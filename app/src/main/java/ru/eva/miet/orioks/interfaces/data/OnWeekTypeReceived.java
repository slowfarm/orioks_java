package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.schedule.WeekType;

public interface OnWeekTypeReceived {
    void onResponse(WeekType schedulers);
    void onFailure(String message);
}
