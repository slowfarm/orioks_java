package ru.eva.miet.orioks.fragment.student.debt;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnDebtsReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.student.Debt;


class RepositoryDebt implements ContractDebt.Repository {
    @Override
    public List<Debt> getDebtList() {
        return StorageHelper.getInstance().getDebts();
    }

    @Override
    public void setDebtList(OnDebtsReceived onDebtsReceived) {
        NetworkHelper.getInstance().getDebts(StorageHelper.getInstance().getAccessToken(), onDebtsReceived);
    }

    @Override
    public void setDebtList(List<Debt> debtList) {
        StorageHelper.getInstance().setDebts(debtList);
    }

    @Override
    public void clearAllTables() {
        StorageHelper.getInstance().clearTables();
    }

    @Override
    public int getDebtId(int position) {
        return StorageHelper.getInstance().getDebts().get(position).getId();
    }

    @Override
    public void deleteToken(OnTokenReceived onTokenReceived) {
        NetworkHelper.getInstance().deleteAccessToken(StorageHelper.getInstance().getAccessToken(), -1, onTokenReceived);
    }
}
