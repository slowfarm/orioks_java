package ru.eva.miet.orioks.model.orioks.teacher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class JournalStudent extends RealmObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("totalScore")
    @Expose
    private Integer totalScore;
    @SerializedName("currentScore")
    @Expose
    private Integer currentScore;
    @SerializedName("currentGrade")
    @Expose
    private Integer currentGrade;
    private int color;
    private String currentGradInString;
    private String currentScoreInString;
    @SerializedName("groupId")
    private int groupId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Integer getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(Integer currentScore) {
        this.currentScore = currentScore;
    }

    public Integer getCurrentGrade() {
        return currentGrade;
    }

    public void setCurrentGrade(Integer currentGrade) {
        this.currentGrade = currentGrade;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getCurrentGradInString() {
        return currentGradInString;
    }

    public void setCurrentGradInString(String currentGradInString) {
        this.currentGradInString = currentGradInString;
    }

    public String getCurrentScoreInString() {
        return currentScoreInString;
    }

    public void setCurrentScoreInString(String currentScoreInString) {
        this.currentScoreInString = currentScoreInString;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
