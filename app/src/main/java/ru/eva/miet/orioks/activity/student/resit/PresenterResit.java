package ru.eva.miet.orioks.activity.student.resit;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnResitsReceived;
import ru.eva.miet.orioks.model.orioks.student.Resit;

class PresenterResit implements ContractResit.Presenter, OnResitsReceived {
    private ContractResit.View mView;
    private ContractResit.Repository mRepository;

    PresenterResit(ContractResit.View mView) {
        this.mView = mView;
        mRepository = new RepositoryResit();
    }

    @Override
    public void setToolbarTitle(int id) {
        mView.setToolbarTitle(mRepository.getToolbarTitle(id));
    }

    @Override
    public void setHeaderData(int id) {
        mView.setHeaderData(mRepository.getDebt(id));
    }

    @Override
    public void getResits(int id) {
        mView.setRecyclerView(mRepository.getResits(id));
        mRepository.getResitList(id, this);
    }

    @Override
    public void onResponse(List<Resit> resitList, int id) {
        if(resitList != null) {
            ConvertHelper.getInstance().convertResit(resitList, id);
            mView.setRecyclerView(resitList);
            mRepository.setResitList(resitList, id);
        }
    }

    @Override
    public void onFailure(String message, int id) {
        mView.showToast("Нет соединения с интернетом");
    }
}
