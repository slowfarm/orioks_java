package ru.eva.miet.orioks.activity.teacher.measure;

import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnMeasureControlReceived;
import ru.eva.miet.orioks.interfaces.data.OnMeasureControlUpdated;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;

class PresenterMeasureControl implements ContractMeasureControl.Presenter, OnMeasureControlReceived, OnMeasureControlUpdated {

    private ContractMeasureControl.View mView;
    private ContractMeasureControl.Repository mRepository;

    PresenterMeasureControl(ContractMeasureControl.View mView) {
        this.mView = mView;
        mRepository = new RepositoryMeasureControl();
    }

    @Override
    public void setToolbarTitle(int studentId) {
        String title = mRepository.getStudentName(studentId);
        if (title.equals(""))
            title = mRepository.getDebtorStudentName(studentId);
        mView.setToolbarTitle(title);
    }

    @Override
    public void getMeasureControl(int disciplineId, int groupId, int studentId) {
        fillMeasureControlListView(mRepository.getLocalMeasureControl(disciplineId, studentId));
        mRepository.getMeasureControl(disciplineId, groupId, studentId, this);
    }

    @Override
    public void onResponse(List<MeasureControl> measureControlList, int disciplineId, int studentId) {
        if (measureControlList != null) {
            for (MeasureControl measureControl : measureControlList) {
                measureControl.setStudentId(studentId);
                measureControl.setGroupId(disciplineId);
            }
            mRepository.setMeasureControl(measureControlList, disciplineId, studentId);
            fillMeasureControlListView(measureControlList);
        }
    }

    private void fillMeasureControlListView(List<MeasureControl> measureControlList) {
        mView.clearListView();
        List<Integer> weekList = new ArrayList<>();
        for (MeasureControl measureControl : measureControlList) {
            weekList.add(measureControl.getWeek());
        }
        List<Integer> listWithoutDuplicates = new ArrayList<>(
                new HashSet<>(weekList));
        Collections.sort(listWithoutDuplicates);
        for (Integer week : listWithoutDuplicates) {
            mView.addWeek(week, measureControlList);
        }
    }

    @Override
    public void addMeasureControl(Integer week, List<MeasureControl> measureControlList) {
        for (int j = 0; j < measureControlList.size(); j++) {
            if (measureControlList.get(j).getWeek().equals(week)) {
                mView.addEvents(ConvertHelper.getInstance().convertMeasureControl(measureControlList.get(j)));
            }
        }
    }

    @Override
    public void checkValue(EditText editText, MeasureControl measureControl, int disciplineId, int groupId, int studentId) {
        if (editText.getText() != null && !editText.getText().toString().isEmpty() && !editText.getText().toString().equals("-")) {
            RequestBody requestBody = new RequestBody();
            Double value = Double.valueOf(editText.getText().toString());
            requestBody.setScore(value);
            if (value < 0) {
                if (value == -1) {
                    mRepository.setMeasureControlValue(requestBody, disciplineId, groupId, studentId, measureControl.getId(), this);
                } else {
                    mView.showToast("Недопустимое значение");
                }
            } else {
                if (value > measureControl.getMaxScore()) {
                    mView.showToast("Недопустимое значение");
                } else {
                    mRepository.setMeasureControlValue(requestBody, disciplineId, groupId, studentId, measureControl.getId(), this);
                }
            }
        } else {
            mView.showToast("Недопустимое значение");
        }
    }

    @Override
    public void onResponse(int disciplineId, int groupId, int studentId) {
        getMeasureControl(disciplineId, groupId, studentId);
        mView.showToast("Оценка обновлена");
    }

    @Override
    public void onFailure(String text) {
        mView.showToast("Дисциплина, группа или студенты не найдены");
    }
}
