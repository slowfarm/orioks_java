package ru.eva.miet.orioks.activity.group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnGroupsReceived;

class PresenterGroupActivity implements ContractGroupActivity.Presenter, OnGroupsReceived {
    private ContractGroupActivity.View mView;
    private ContractGroupActivity.Repository mRepository;

    PresenterGroupActivity(ContractGroupActivity.View mView) {
        this.mView = mView;
        this.mRepository = new RepositoryGroupActivity();
    }

    @Override
    public void getGroups() {
        mRepository.getGroups(this);
    }

    @Override
    public void onResponse(String[] groups) {
        List<String> groupList = new ArrayList<>();
        Collections.addAll(groupList, groups);
        mView.setRecyclerView(groupList);
    }

    @Override
    public void onFailure(String message) {
        mView.showToast(message);
    }
}
