package ru.eva.miet.orioks.fragment.student.debt;

import android.opengl.Visibility;
import android.view.View;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnDebtsReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.student.Debt;


class PresenterDebt implements ContractDebt.Presenter, OnDebtsReceived, OnTokenReceived {
    private ContractDebt.View mView;
    private ContractDebt.Repository mRepository;

    PresenterDebt(ContractDebt.View mView) {
        this.mView = mView;
        mRepository = new RepositoryDebt();
    }

    @Override
    public void getDebtList() {
        mView.setRecyclerView(ConvertHelper.getInstance().convertDebt(mRepository.getDebtList()));
    }

    @Override
    public void setDebtList() {
        mRepository.setDebtList(this);
    }

    @Override
    public void onDebtClick(int position) {
        mView.startEventActivity(mRepository.getDebtId(position));
    }

    @Override
    public void onResponse(List<Debt> debtList) {
        if(debtList != null) {
            mRepository.setDebtList(ConvertHelper.getInstance().convertDebt(debtList));
            mView.addRecyclerVIewItems(debtList, debtList.size() == 0 ? View.VISIBLE : View.GONE);
        } else {
            mView.showToast("Токен аннулирован");
            mRepository.deleteToken(this);
        }
        mView.unsetRefreshing();
    }

    @Override
    public void onResponse(AccessToken accessToken, int position) {
        finishApp();
    }

    @Override
    public void onFailure(String message) {
        mView.showToast("Нет соединения с интернетом");
        mView.unsetRefreshing();
    }

    private void finishApp(){
        mRepository.clearAllTables();
        mView.finishActivity();
    }
}
