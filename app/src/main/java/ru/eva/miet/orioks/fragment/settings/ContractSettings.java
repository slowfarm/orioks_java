package ru.eva.miet.orioks.fragment.settings;

import android.content.Context;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnPusherReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.Security;

class ContractSettings {
    interface View {

        void showToast(String text);

        void setAdapter(List<Security> allActiveTokens);

        void finishActivity();

        void unsetRefreshing();

        void notifyItemRemoved(int position);

        void showPusherDialog();
    }

    interface Presenter {

        void deleteActiveToken(Security token, int position, Context context);

        void getAllActiveTokens();

        void refreshActiveTokens();

        void logOut(Context context);

        void unregisterPusher(Context context);
    }

    interface Repository {

        void deleteActiveToken(Security token, int position, OnTokenReceived onTokenReceived);

        void getAllActiveTokens(OnAllAccessTokensReceived onAllAccessTokensReceived);

        void setAllActiveTokens(List<Security> tokens);

        void clearAllTables();

        List<Security> getAllActiveLocalTokens();

        void deleteActiveLocalToken(AccessToken token);

        void deleteCurrentToken(OnTokenReceived onTokenReceived);

        void unregisterPusher(RequestBody body, OnPusherReceived onPusherReceived);

        String getAccessToken();
    }
}
