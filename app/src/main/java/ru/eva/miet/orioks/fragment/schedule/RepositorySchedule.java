package ru.eva.miet.orioks.fragment.schedule;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.StorageHelper;


class RepositorySchedule implements ContractSchedule.Repository {

    @Override
    public List<String> getLocalGroups() {
        return StorageHelper.getInstance().getGroups();
    }

    @Override
    public void removeGroup(String group) {
        StorageHelper.getInstance().removeGroup(group);
    }

    @Override
    public void addLocalGroup(String group) {
        StorageHelper.getInstance().addLocalGroup(group);
    }
}
