package ru.eva.miet.orioks.activity.student.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnEventsReceived;
import ru.eva.miet.orioks.model.orioks.student.Event;

class PresenterEvent implements ContractEvent.Presenter, OnEventsReceived {
    private ContractEvent.View mView;
    private ContractEvent.Repository mRepository;

    PresenterEvent(ContractEvent.View mView) {
        this.mView = mView;
        mRepository = new RepositoryEvent();
    }

    @Override
    public void setToolbarTitle(int id) {
        mView.setToolbarTitle(mRepository.getToolbarTitle(id));
    }

    @Override
    public void setHeaderData(int id) {
        mView.setHeaderData(mRepository.getDiscipline(id));
    }

    @Override
    public void getEvents(int id) {
        fillListView(mRepository.getEvents(id));
        mRepository.getEventList(id, this);
    }

    @Override
    public void onResponse(List<Event> eventList, int id) {
        if(eventList != null) {
            for (Event event : eventList)
                event.setId(id);
            fillListView(eventList);
            mRepository.setEventList(eventList, id);
        }
    }

    @Override
    public void addEvents(Integer week, List<Event> eventList) {
        for (int j = 0; j < eventList.size(); j++) {
            if (eventList.get(j).getWeek().equals(week)) {
                mView.addEvents(ConvertHelper.getInstance().convertEvent(eventList.get(j)));
            }
        }
    }

    private void fillListView(List<Event> eventList) {
        if(eventList.size() != 0) {
            mView.setNoEventTextViewVisibility(false);
            mView.clearListView();
            List<Integer> weekList = new ArrayList<>();
            for (Event event : eventList) {
                weekList.add(event.getWeek());
            }
            List<Integer> listWithoutDuplicates = new ArrayList<>(
                    new HashSet<>(weekList));
            Collections.sort(listWithoutDuplicates);
            for (Integer week : listWithoutDuplicates)
                mView.addWeek(week, eventList);
        } else {
            mView.setNoEventTextViewVisibility(true);
        }
    }

    @Override
    public void onFailure(String message, int id) {
        mView.showToast("Нет соединения с интернетом");
    }
}
