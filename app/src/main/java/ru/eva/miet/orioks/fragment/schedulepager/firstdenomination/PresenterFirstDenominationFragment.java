package ru.eva.miet.orioks.fragment.schedulepager.firstdenomination;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.model.schedule.Data;


public class PresenterFirstDenominationFragment implements ContractFirstDenominationFragment.Presenter{
    private ContractFirstDenominationFragment.View mView;
    private ContractFirstDenominationFragment.Repository mRepository;


    PresenterFirstDenominationFragment(ContractFirstDenominationFragment.View mView) {
        this.mView = mView;
        mRepository = new RepositoryFirstDenominationFragment();
    }

    @Override
    public void getSchedule(String group) {
        List<Data> dataList = ConvertHelper.getInstance().scheduleWeek(mRepository.getSchedule(1, group));
        mView.setAdapter(dataList);
    }
}
