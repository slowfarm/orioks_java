package ru.eva.miet.orioks.adapter.teacher;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnEventItemClickListener;
import ru.eva.miet.orioks.model.ChildDiscipline;
import ru.eva.miet.orioks.model.ParentDiscipline;

public class TeacherEventFragmentAdapter extends RecyclerView.Adapter<TeacherEventFragmentAdapter.ViewHolder> {

    private List<ParentDiscipline> parentDisciplines;
    private OnEventItemClickListener onItemClickListener;
    private List<ParentDiscipline> filteredData = new ArrayList<>();

    public TeacherEventFragmentAdapter(List<ParentDiscipline> parentDisciplines) {
        this.parentDisciplines = parentDisciplines;
        filteredData.addAll(parentDisciplines);
    }

    public void addItems(List<ParentDiscipline> parentDisciplines) {
        this.parentDisciplines = parentDisciplines;
        filteredData.clear();
        filteredData.addAll(parentDisciplines);
        notifyDataSetChanged();
    }

    public void filterData(String constraint) {
        mapData(constraint, parentDisciplines);
    }

    @Override
    public TeacherEventFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_event_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TeacherEventFragmentAdapter.ViewHolder holder, int position) {
        ParentDiscipline parentDiscipline = filteredData.get(position);
        holder.eventTextView.setText(parentDiscipline.getName());
        if (parentDiscipline.isParent()) {
            holder.eventTextView.setTypeface(null, Typeface.BOLD);
        } else {
            holder.eventTextView.setTypeface(null, Typeface.NORMAL);
        }
        ((ViewGroup.MarginLayoutParams) holder.itemView.getLayoutParams()).leftMargin = parentDiscipline.getPadding();
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    public void setOnItemClickListener(OnEventItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView eventTextView;

        ViewHolder(View itemView) {
            super(itemView);
            eventTextView = itemView.findViewById(R.id.event_text_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                eventOpener(filteredData.get(getAdapterPosition()), getAdapterPosition());
            }
        }
    }

    private void eventOpener(ParentDiscipline discipline, int adapterPosition) {
        if (discipline.getChildDisciplines().size() != 0) {
            if (discipline.isOpen()) {
                closeDiscipline(discipline.getChildDisciplines(), adapterPosition);
                discipline.setOpen(false);
            } else {
                openDiscipline(discipline.getChildDisciplines(), adapterPosition);
                discipline.setOpen(true);
            }
        } else {
            onItemClickListener.onClick(discipline.getIdDiscipline(), discipline.getIdGroup());
        }
    }

    private void closeDiscipline(List<ChildDiscipline> childrenList, int adapterPosition) {
        int childrenListMaxRange = adapterPosition + childrenList.size() + 1;
        for (int i = adapterPosition + 1; i < childrenListMaxRange; i++) {
            filteredData.remove(i);
            childrenListMaxRange--;
            i--;
        }
        notifyItemRangeRemoved(adapterPosition + 1, childrenList.size());
    }

    private void openDiscipline(List<ChildDiscipline> childrenList, int adapterPosition) {
        int tempSize = getItemCount();
        List<ParentDiscipline> tempDiscipline = new ArrayList<>();
        for (ChildDiscipline child : childrenList) {
            ParentDiscipline temp = new ParentDiscipline();
            temp.setName(child.getName());
            temp.setParent(false);
            temp.setPadding(48);
            temp.setIdGroup(child.getIdGroup());
            temp.setIdDiscipline(child.getIdDiscipline());
            tempDiscipline.add(temp);
        }
        filteredData.addAll(adapterPosition + 1, tempDiscipline);
        notifyItemRangeInserted(adapterPosition + 1, childrenList.size());
        if(adapterPosition == tempSize - 1)
            onItemClickListener.scrollToBottom();
    }

    private void  mapData(String constraint,  List<ParentDiscipline> list) {
        List<ChildDiscipline> cList = new ArrayList<>();
        for (ParentDiscipline parent : list) {
            for (ChildDiscipline child : parent.getChildDisciplines()) {
                if (child.getName().toLowerCase().contains(constraint.toLowerCase())) {
                    cList.add(child);
                }
            }
        }
        searchDiscipline(cList);
    }

    private void searchDiscipline(List<ChildDiscipline> childrenList) {
        List<ParentDiscipline> tempDiscipline = new ArrayList<>();
        for (ChildDiscipline child : childrenList) {
            ParentDiscipline temp = new ParentDiscipline();
            temp.setName(child.getName());
            temp.setParent(false);
            temp.setIdGroup(child.getIdGroup());
            temp.setIdDiscipline(child.getIdDiscipline());
            tempDiscipline.add(temp);
        }
        filteredData.clear();
        filteredData.addAll(tempDiscipline);
        notifyDataSetChanged();
    }
}