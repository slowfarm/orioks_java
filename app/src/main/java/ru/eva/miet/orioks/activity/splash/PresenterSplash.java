package ru.eva.miet.orioks.activity.splash;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.interfaces.data.OnWeekTypeReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.schedule.WeekType;


public class PresenterSplash implements
        ContractSplash.Presenter,
        OnTokenReceived,
        OnAllAccessTokensReceived,
        OnWeekTypeReceived,
        OnRolesReceived,
        OnSemestersReceived {
    private ContractSplash.View mView;
    private ContractSplash.Repository mRepository;

    PresenterSplash(ContractSplash.View mView) {
        this.mView = mView;
        mRepository = new RepositorySplash();
    }

    @Override
    public void checkAccessToken() {
        if(mRepository.getAccessToken() != null) {
            mRepository.getTypeOfWeek(this);
        } else {
            mView.startAuthActivity();
        }
    }

    @Override
    public void onResponse(WeekType weekType) {
        if (weekType != null) {
            mRepository.setWeekType(weekType);
            mRepository.getAllActiveTokens(this);
        } else {
            mView.showToast("Токен аннулирован");
            mRepository.deleteToken(this);
        }
    }


    @Override
    public void onResponse(AccessToken accessToken, int position) {
        finishApp();
    }

    @Override
    public void onResponse(List<Security> tokens) {
        mRepository.setAllActiveTokens(tokens);
        mRepository.getRoles(this);
    }

    @Override
    public void onResponse(Roles roles) {
        mRepository.setRoles(roles);
        mRepository.getSemester(this);
    }

    @Override
    public void onResponse(Semester semester) {
        mRepository.setSemester(semester);
        mView.startMainActivity();
    }

    @Override
    public void onFailure(String message) {
        mView.showToast("Нет соединения с интернетом");
        mView.startMainActivity();
    }

    private void finishApp(){
        mRepository.clearAllTables();
        mView.startAuthActivity();
    }
}
