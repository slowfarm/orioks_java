package ru.eva.miet.orioks.model;

import java.util.ArrayList;
import java.util.List;

public class ParentDiscipline {

    private String name;
    private boolean isOpen = false;
    private boolean isParent = true;
    private int padding = 16;
    private int idGroup;
    private int idDiscipline;
    private List<ChildDiscipline> childDisciplines = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ChildDiscipline> getChildDisciplines() {
        return childDisciplines;
    }

    public void setChildDisciplines(List<ChildDiscipline> childDisciplines) {
        this.childDisciplines = childDisciplines;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getPadding() {
        return padding;
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public int getIdDiscipline() {
        return idDiscipline;
    }

    public void setIdDiscipline(int idDiscipline) {
        this.idDiscipline = idDiscipline;
    }
}
