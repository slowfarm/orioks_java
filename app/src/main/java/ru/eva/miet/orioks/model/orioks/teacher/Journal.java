package ru.eva.miet.orioks.model.orioks.teacher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Journal extends RealmObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("students")
    @Expose
    private RealmList<JournalStudent> students = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public RealmList<JournalStudent> getStudents() {
        return students;
    }

    public void setStudents(RealmList<JournalStudent> students) {
        this.students = students;
    }
}
