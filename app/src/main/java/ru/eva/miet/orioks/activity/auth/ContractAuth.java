package ru.eva.miet.orioks.activity.auth;

import android.content.Context;
import android.text.Editable;

import java.util.List;

import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnPusherReceived;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.interfaces.data.OnWeekTypeReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.schedule.WeekType;

public class ContractAuth {
    interface View {
        void startActivity();

        void showToast(String text);

        void setLoginButtonEnable(boolean isEnable);

        void setStudentNumberLayoutError(String error);

        void setLoginLayoutError(String error);

        void setLogin(String login);

        void showPusherDialog();
    }

    interface Presenter {
        void onLoginButtonClick(Editable login, Editable password, Context context);

        void onStudentNumberTextChanged(Editable text);

        void onPasswordTextChangeListener(Editable text);

        void setLogin(Context context);

        void allowPushService(Context context);
    }

    interface Repository {
        void getAccessToken(String encodedString, OnTokenReceived onTokenReceived);

        void setAccessToken(AccessToken accessToken);

        void getAllActiveTokens(OnAllAccessTokensReceived onAllAccessTokensReceived);

        void setAllActiveTokens(List<Security> tokens);

        void setLogin(String login, Context context);

        String getLogin(Context context);

        void registerPusher(RequestBody body, OnPusherReceived onPusherReceived);

        String getFirebaseToken(Context context);

        void deleteAccessToken();

        void getTypeOfWeek(OnWeekTypeReceived onWeekTypeReceived);

        void setTypeOfWeek(WeekType typeOfWeek);

        void getRoles(OnRolesReceived onRolesReceived);

        void setRoles(Roles roles);

        void getSemesters(OnSemestersReceived onSemestersReceived);

        void setSemester(Semester semester);

        Roles getLocalRoles();

//        void getTypeOfWeek(OnWeekTypeReceived onWeekTypeReceived);

//        void getTimingTable(OnTimeTableReceived onTimeTableReceived);

//        void setTypeOfWeek(WeekType typeOfWeek);
    }
}
