package ru.eva.miet.orioks.interfaces.data;

import ru.eva.miet.orioks.model.orioks.Semester;

public interface OnSemestersReceived {
    void onResponse(Semester semester);
    void onFailure(String message);
}
