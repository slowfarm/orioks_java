package ru.eva.miet.orioks.activity.auth;

import android.content.Context;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnPusherReceived;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.interfaces.data.OnWeekTypeReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.schedule.WeekType;

public class RepositoryAuth implements ContractAuth.Repository {
    @Override
    public void getAccessToken(String encodedString, OnTokenReceived onTokenReceived) {
        NetworkHelper.getInstance().getAccessToken(encodedString, onTokenReceived);
    }

    @Override
    public void setAccessToken(AccessToken accessToken) {
        StorageHelper.getInstance().setAccessToken(accessToken);
    }


    @Override
    public void getAllActiveTokens(OnAllAccessTokensReceived onAllAccessTokensReceived) {
        NetworkHelper.getInstance().getAllActiveTokens(StorageHelper.getInstance().getAccessToken(), onAllAccessTokensReceived);
    }

    @Override
    public void setAllActiveTokens(List<Security> tokens) {
        StorageHelper.getInstance().setAllActiveTokens(tokens);
    }

    @Override
    public void setLogin(String login, Context context) {
        StorageHelper.getInstance().setLogin(login, context);
    }

    @Override
    public String getLogin(Context context) {
        return StorageHelper.getInstance().getLogin(context);
    }

    @Override
    public void registerPusher(RequestBody requestBody, OnPusherReceived onPusherReceived) {
        NetworkHelper.getInstance().registerPusher(StorageHelper.getInstance().getAccessToken(), requestBody, onPusherReceived);
    }

    @Override
    public String getFirebaseToken(Context context) {
        return StorageHelper.getInstance().getFirebaseToken(context);
    }

    @Override
    public void deleteAccessToken() {
        StorageHelper.getInstance().deleteLocalToken();
    }

    @Override
    public void getTypeOfWeek(OnWeekTypeReceived onWeekTypeReceived) {
        NetworkHelper.getInstance().getWeekType(StorageHelper.getInstance().getAccessToken(), onWeekTypeReceived);
    }

    @Override
    public void setTypeOfWeek(WeekType weekType) {
        StorageHelper.getInstance().setTypeOfWeek(weekType);
    }

    @Override
    public void getRoles(OnRolesReceived onRolesReceived) {
        NetworkHelper.getInstance().getRoles(StorageHelper.getInstance().getAccessToken(), onRolesReceived);
    }

    @Override
    public void setRoles(Roles roles) {
        StorageHelper.getInstance().setRoles(roles);
    }

    @Override
    public void getSemesters(OnSemestersReceived onSemestersReceived) {
        NetworkHelper.getInstance().getSemesters(StorageHelper.getInstance().getAccessToken(), onSemestersReceived);
    }

    @Override
    public void setSemester(Semester semester) {
        StorageHelper.getInstance().setSemester(semester);
    }

    @Override
    public Roles getLocalRoles() {
        return StorageHelper.getInstance().getRoles();
    }

//    @Override
//    public void getTimingTable(OnTimeTableReceived onTimeTableReceived) {
//        RetrofitHelper.getInstance().setOnTimeTableReceived(onTimeTableReceived);
//        RetrofitHelper.getInstance().getTimeTable(StorageHelper.getInstance().getAccessToken());
//    }

//    @Override
//    public void setTimingTable(Timing timingTable) {
//        StorageHelper.getInstance().setTimingTable(timingTable);
//    }
}
