package ru.eva.miet.orioks.model.orioks;

import com.google.gson.annotations.SerializedName;

public class RequestBody {

    @SerializedName("deviceUuid")
    private String deviceUuid;
    @SerializedName("pushKey")
    private String pushKey;
    @SerializedName("deviceType")
    private Integer deviceType;
    @SerializedName("score")
    private Double score;

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public String getPushKey() {
        return pushKey;
    }

    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

}
