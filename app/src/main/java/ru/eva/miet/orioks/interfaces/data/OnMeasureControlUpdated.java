package ru.eva.miet.orioks.interfaces.data;

public interface OnMeasureControlUpdated {
    void onResponse(int disciplineId, int groupId, int studentId);
    void onFailure(String text);
}
