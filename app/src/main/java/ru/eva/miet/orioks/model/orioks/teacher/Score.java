package ru.eva.miet.orioks.model.orioks.teacher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Score {
    @SerializedName("score")
    @Expose
    private Double score;
    @SerializedName("error")
    @Expose
    private String error;

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
