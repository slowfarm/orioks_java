package ru.eva.miet.orioks.fragment.settings;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnPusherReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.Security;


class RepositorySettings implements ContractSettings.Repository {

    @Override
    public void deleteActiveToken(Security token, int position, OnTokenReceived onTokenReceived) {
        NetworkHelper.getInstance().deleteAccessToken(token.getToken(), position, onTokenReceived);
    }

    @Override
    public void getAllActiveTokens(OnAllAccessTokensReceived onAllAccessTokensReceived) {
        NetworkHelper.getInstance().getAllActiveTokens(StorageHelper.getInstance().getAccessToken(), onAllAccessTokensReceived);
    }

    @Override
    public void setAllActiveTokens(List<Security> tokens) {
        StorageHelper.getInstance().setAllActiveTokens(tokens);
    }

    @Override
    public void clearAllTables() {
        StorageHelper.getInstance().clearTables();
    }

    @Override
    public List<Security> getAllActiveLocalTokens() {
        return StorageHelper.getInstance().getAllActiveTokens();
    }

    @Override
    public void deleteActiveLocalToken(AccessToken token) {
        StorageHelper.getInstance().deleteActiveToken(token);
    }

    @Override
    public void deleteCurrentToken(OnTokenReceived onTokenReceived) {
        NetworkHelper.getInstance().deleteAccessToken(StorageHelper.getInstance().getAccessToken(), -1, onTokenReceived);
    }

    @Override
    public void unregisterPusher(RequestBody requestBody, OnPusherReceived onPusherReceived) {
        NetworkHelper.getInstance().unregisterPusher(StorageHelper.getInstance().getAccessToken(), requestBody, onPusherReceived);
    }

    @Override
    public String getAccessToken() {
        return StorageHelper.getInstance().getAccessToken();
    }
}
