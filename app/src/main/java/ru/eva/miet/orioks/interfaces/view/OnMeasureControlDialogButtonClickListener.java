package ru.eva.miet.orioks.interfaces.view;

import androidx.appcompat.widget.AppCompatEditText;

import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;

public interface OnMeasureControlDialogButtonClickListener {
    void onPositiveButtonClickListener(AppCompatEditText editText, MeasureControl measureControl);
}
