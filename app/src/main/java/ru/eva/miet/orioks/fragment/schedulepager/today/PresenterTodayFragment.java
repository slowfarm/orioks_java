package ru.eva.miet.orioks.fragment.schedulepager.today;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.model.schedule.Data;


class PresenterTodayFragment implements ContractTodayFragment.Presenter{
    private ContractTodayFragment.View mView;
    private ContractTodayFragment.Repository mRepository;


    PresenterTodayFragment(ContractTodayFragment.View mView) {
        this.mView = mView;
        mRepository = new RepositoryTodayFragment();
    }

    @Override
    public void getSchedule(String group) {
        int currentWeek = ConvertHelper.getInstance().getCurrentWeek(mRepository.getCurrentWeekType().getSemesterStart());
        int currentDayOfWeek = ConvertHelper.getInstance().getCurrentDayOfWeek();
        List<Data> dataList = ConvertHelper.getInstance().scheduleDay(
                mRepository.getSchedule(currentWeek, currentDayOfWeek, group));
        mView.setAdapter(dataList);
    }


}
