package ru.eva.miet.orioks;

import androidx.appcompat.app.AppCompatDelegate;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.http.OrioksAPI;
import ru.eva.miet.orioks.interfaces.http.ScheduleAPI;

public class App extends android.app.Application {
    private static OrioksAPI orioksApi;
    private static ScheduleAPI scheduleAPI;

    @Override
    public void onCreate() {
        super.onCreate();

        if(StorageHelper.getInstance().getIsNightThemeSelected(this)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);


        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

        OkHttpClient httpClient = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://orioks.miet.ru/")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        orioksApi = retrofit.create(OrioksAPI.class);

        retrofit = new Retrofit.Builder()
                .baseUrl("https:/miet.ru/")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        scheduleAPI = retrofit.create(ScheduleAPI.class);
    }

    public static OrioksAPI getApi() {
        return orioksApi;
    }

    public static ScheduleAPI getScheduleAPI() {return scheduleAPI;}
}