package ru.eva.miet.orioks.fragment.settings;

import android.content.Context;
import android.provider.Settings;

import com.google.gson.Gson;

import java.util.List;

import okhttp3.MediaType;
import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnPusherReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.AccessToken;
import ru.eva.miet.orioks.model.orioks.RequestBody;
import ru.eva.miet.orioks.model.orioks.Security;


class PresenterSettings implements ContractSettings.Presenter,
        OnTokenReceived,
        OnAllAccessTokensReceived,
        OnPusherReceived {
    private ContractSettings.View mView;
    private ContractSettings.Repository mRepository;

    PresenterSettings(ContractSettings.View mView) {
        this.mView = mView;
        mRepository = new RepositorySettings();
    }

    @Override
    public void logOut(Context context) {
        mView.showPusherDialog();
    }

    @Override
    public void unregisterPusher(Context context) {
        RequestBody requestBody = new RequestBody();
        requestBody.setDeviceUuid(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        mRepository.unregisterPusher(requestBody,this);
    }

    @Override
    public void onResponse() {
        mRepository.deleteCurrentToken(this);
    }

    @Override
    public void onFailure() {
        mRepository.deleteCurrentToken(this);
    }

    @Override
    public void deleteActiveToken(Security token, int position, Context context) {
        if(token.getToken().equals(mRepository.getAccessToken())) {
            logOut(context);
        }
        else {
            mRepository.deleteActiveToken(token, position, this);
        }
    }

    @Override
    public void getAllActiveTokens() {
        mView.setAdapter(ConvertHelper.getInstance().convertTokens(mRepository.getAllActiveLocalTokens(), mRepository.getAccessToken()));
    }

    @Override
    public void refreshActiveTokens() {
        mRepository.getAllActiveTokens(this);
    }

    @Override
    public void onResponse(AccessToken accessToken, int position) {
        mRepository.deleteActiveLocalToken(accessToken);
        if(position != -1) {
            mView.notifyItemRemoved(position);
        }
        else
            finishApp();
        //mRepository.getAllActiveTokens(this);
    }

    @Override
    public void onResponse(List<Security> tokens) {
        mView.unsetRefreshing();
        if(tokens != null) {
            mRepository.setAllActiveTokens(tokens);
            mView.setAdapter(ConvertHelper.getInstance().convertTokens(mRepository.getAllActiveLocalTokens(), mRepository.getAccessToken()));
        }
        else {
            mView.showToast("Токен анулирован");
            finishApp();
        }
    }

    @Override
    public void onFailure(String message) {
        mView.showToast("Нет соединения с интернетом");
        mView.unsetRefreshing();
    }



    private void finishApp(){
        mRepository.clearAllTables();
        mView.finishActivity();
    }
}
