package ru.eva.miet.orioks.fragment.schedulepager.firstnumerator;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.model.schedule.Data;

public class PresenterFirstNumeratorFragment implements ContractFirstNumeratorFragment.Presenter{
    private ContractFirstNumeratorFragment.View mView;
    private ContractFirstNumeratorFragment.Repository mRepository;


    PresenterFirstNumeratorFragment(ContractFirstNumeratorFragment.View mView) {
        this.mView = mView;
        mRepository = new RepositoryFirstNumeratorFragment();
    }

    @Override
    public void getSchedule(String group) {
        List<Data> dataList = ConvertHelper.getInstance().scheduleWeek(mRepository.getSchedule(0, group));
        mView.setAdapter(dataList);
    }
}
