package ru.eva.miet.orioks.activity.splash;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnAllAccessTokensReceived;
import ru.eva.miet.orioks.interfaces.data.OnRolesReceived;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.interfaces.data.OnWeekTypeReceived;
import ru.eva.miet.orioks.model.orioks.Security;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.orioks.Roles;
import ru.eva.miet.orioks.model.schedule.WeekType;


public class RepositorySplash implements ContractSplash.Repository {

    @Override
    public String getAccessToken() {
        return StorageHelper.getInstance().getAccessToken();
    }

    @Override
    public WeekType getCurrentWeekType() {
        return StorageHelper.getInstance().getWeekType();
    }

    @Override
    public void getTypeOfWeek(OnWeekTypeReceived onWeekTypeReceived) {
        NetworkHelper.getInstance().getWeekType(StorageHelper.getInstance().getAccessToken(), onWeekTypeReceived);
    }

    @Override
    public void setWeekType(WeekType weekType) {
        StorageHelper.getInstance().setTypeOfWeek(weekType);
    }

    @Override
    public void getRoles(OnRolesReceived onRolesReceived) {
        NetworkHelper.getInstance().getRoles(StorageHelper.getInstance().getAccessToken(), onRolesReceived);
    }

    @Override
    public void setRoles(Roles roles) {
        StorageHelper.getInstance().setRoles(roles);
    }

    @Override
    public void getSemester(OnSemestersReceived onSemestersReceived) {
        NetworkHelper.getInstance().getSemesters(StorageHelper.getInstance().getAccessToken(), onSemestersReceived);
    }

    @Override
    public void setSemester(Semester semester) {
        StorageHelper.getInstance().setSemester(semester);
    }

    @Override
    public void setStudent(Student student) {
        StorageHelper.getInstance().setStudent(student);
    }

    @Override
    public void deleteToken(OnTokenReceived onTokenReceived) {
        NetworkHelper.getInstance().deleteAccessToken(StorageHelper.getInstance().getAccessToken(), -1, onTokenReceived);
    }

    @Override
    public void clearAllTables() {
        StorageHelper.getInstance().clearTables();
    }

    @Override
    public void getAllActiveTokens(OnAllAccessTokensReceived onAllAccessTokensReceived) {
        NetworkHelper.getInstance().getAllActiveTokens(StorageHelper.getInstance().getAccessToken(), onAllAccessTokensReceived);
    }

    @Override
    public void setAllActiveTokens(List<Security> tokens) {
        StorageHelper.getInstance().setAllActiveTokens(tokens);
    }
}
