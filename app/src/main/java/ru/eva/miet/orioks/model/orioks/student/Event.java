package ru.eva.miet.orioks.model.orioks.student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Event extends RealmObject {
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("currentScore")
    @Expose
    private Double currentScore;
    @SerializedName("maxScore")
    @Expose
    private Double maxScore;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("week")
    @Expose
    private Integer week;
    @SerializedName("id")
    @Expose
    private Integer id;
    private String currentGradeInString;
    private String maxGradeInString;
    private int color;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Double getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(Double currentScore) {
        this.currentScore = currentScore;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public String getCurrentGradeInString() {
        return currentGradeInString;
    }

    public void setCurrentGradeInString(String currentGradeInString) {
        this.currentGradeInString = currentGradeInString;
    }

    public String getMaxGradeInString() {
        return maxGradeInString;
    }

    public void setMaxGradeInString(String maxGradeInString) {
        this.maxGradeInString = maxGradeInString;
    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}