package ru.eva.miet.orioks.fragment.schedulepager.firstdenomination;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.model.schedule.Data;


public class RepositoryFirstDenominationFragment implements ContractFirstDenominationFragment.Repository {
    @Override
    public List<Data> getSchedule(int week, String group) {
        return StorageHelper.getInstance().getSchedulersDataCurrentWeek(week, group);
    }
}
