package ru.eva.miet.orioks.model.schedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class WeekType extends RealmObject {
    @SerializedName("semesterStart")
    @Expose
    private String semesterStart;
    @SerializedName("sessionStart")
    @Expose
    private String sessionStart;
    @SerializedName("sessionEnd")
    @Expose
    private String sessionEnd;
    @SerializedName("nextSemesterStart")
    @Expose
    private String nextSemesterStart;

    public String getSemesterStart() {
        return semesterStart;
    }

    public void setSemesterStart(String semesterStart) {
        this.semesterStart = semesterStart;
    }

    public String getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(String sessionStart) {
        this.sessionStart = sessionStart;
    }

    public String getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(String sessionEnd) {
        this.sessionEnd = sessionEnd;
    }

    public String getNextSemesterStart() {
        return nextSemesterStart;
    }

    public void setNextSemesterStart(String nextSemesterStart) {
        this.nextSemesterStart = nextSemesterStart;
    }
}
