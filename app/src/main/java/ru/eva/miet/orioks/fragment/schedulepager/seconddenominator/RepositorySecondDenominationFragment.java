package ru.eva.miet.orioks.fragment.schedulepager.seconddenominator;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.model.schedule.Data;


public class RepositorySecondDenominationFragment implements ContractSecondDenominationFragment.Repository {
    @Override
    public List<Data> getSchedule(int week, String group) {
        return StorageHelper.getInstance().getSchedulersDataCurrentWeek(week, group);
    }
}
