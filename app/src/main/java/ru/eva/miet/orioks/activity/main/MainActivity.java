package ru.eva.miet.orioks.activity.main;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.google.android.material.navigation.NavigationView;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.fragment.schedule.ScheduleFragment;
import ru.eva.miet.orioks.fragment.settings.SettingsFragment;
import ru.eva.miet.orioks.fragment.student.debt.DebtFragment;
import ru.eva.miet.orioks.fragment.student.discipline.DisciplineFragment;
import ru.eva.miet.orioks.fragment.roles.RolesFragment;
import ru.eva.miet.orioks.fragment.teacher.event.EventFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ContractMain.View {

    private AppCompatTextView nameTextView;
    private AppCompatTextView numberTextView;
    private AppCompatTextView groupTextView;
    private AppCompatTextView weekTextView;
    private CircularProgressBar progressBar;
    private AppCompatTextView weekIndicatorTextView;

    private DisciplineFragment disciplineFragment;
    private DebtFragment debtFragment;
    private SettingsFragment settingsFragment;
    private ScheduleFragment scheduleFragment;
    private EventFragment eventFragment;
    private RolesFragment rolesFragment;

    private FragmentTransaction fTrans;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private Switch themeSwitch;
    private DrawerLayout drawer;

    private ContractMain.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new PresenterMain(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        nameTextView = navigationView.getHeaderView(0).findViewById(R.id.name_text_view);
        numberTextView = navigationView.getHeaderView(0).findViewById(R.id.number_text_view);
        groupTextView = navigationView.getHeaderView(0).findViewById(R.id.group_text_view);
        weekTextView = navigationView.getHeaderView(0).findViewById(R.id.week_text_view);
        weekIndicatorTextView = navigationView.getHeaderView(0).findViewById(R.id.week_indicator_text_view);
        progressBar = navigationView.getHeaderView(0).findViewById(R.id.progress_bar);

        mPresenter.setTypeOfWeek();
        mPresenter.checkRoles(this);
        mPresenter.initSwitchThemeItem(this);
    }

    @Override
    public void initStudentViews() {
        navigationView.inflateMenu(R.menu.activity_main_student_drawer);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_study);

        disciplineFragment = new DisciplineFragment();
        settingsFragment = new SettingsFragment();
        rolesFragment = new RolesFragment();
        scheduleFragment = new ScheduleFragment();
        debtFragment = new DebtFragment();

        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.frame_layout, disciplineFragment).commit();
    }

    @Override
    public void initTeacherViews() {
        navigationView.inflateMenu(R.menu.activity_main_teacher_drawer);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_event);

        settingsFragment = new SettingsFragment();
        rolesFragment = new RolesFragment();
        scheduleFragment = new ScheduleFragment();
        eventFragment = new EventFragment();

        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.frame_layout, eventFragment).commit();
    }

    @Override
    public void initSwitchThemeItem(boolean isDarkThemeSelected) {
        MenuItem menuItem = navigationView.getMenu().findItem(R.id.nav_theme_switch);
        themeSwitch = menuItem.getActionView().findViewById(R.id.theme_switch);
        themeSwitch.setChecked(isDarkThemeSelected);
        themeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mPresenter.changeTheme(this, isChecked);
            navigationView.getMenu().getItem(0).setChecked(true);
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        fTrans = getFragmentManager().beginTransaction();
        switch (item.getItemId()) {
            case R.id.nav_study:
                replaceFragment("Обучение", disciplineFragment);
                break;
            case R.id.nav_debt:
                replaceFragment("Долги", debtFragment);
                break;
            case R.id.nav_event:
                replaceFragment("Дисциплины", eventFragment);
                break;
            case R.id.nav_schedule:
                replaceFragment("Расписание", scheduleFragment);
                break;
            case R.id.nav_settings:
                replaceFragment("Настройки", settingsFragment);
                break;
            case R.id.nav_roles:
                replaceFragment("Сменить роль", rolesFragment);
                break;
            case R.id.nav_theme_switch:
                themeSwitch.toggle();
                break;
        }
        return true;
    }

    private void replaceFragment(String title, Fragment fragment) {
        toolbar.setTitle(title);
        fTrans.replace(R.id.frame_layout, fragment);
        fTrans.commit();
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void setPerson(String name, String number, String group) {
        nameTextView.setText(name);
        numberTextView.setText(number);
        groupTextView.setText(group);
    }

    @Override
    public void setWeekType(String currentWeek, String weekType, float progress) {
        weekIndicatorTextView.setText(currentWeek);
        weekTextView.setText(weekType);
        progressBar.setProgress(progress);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
