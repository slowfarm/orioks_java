package ru.eva.miet.orioks.model;

import ru.eva.miet.orioks.model.orioks.teacher.Attributes;

public class ChildDiscipline {

    private String name;
    private int idGroup;
    private int idDiscipline;
    private Attributes attributes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public int getIdDiscipline() {
        return idDiscipline;
    }

    public void setIdDiscipline(int idDiscipline) {
        this.idDiscipline = idDiscipline;
    }
}
