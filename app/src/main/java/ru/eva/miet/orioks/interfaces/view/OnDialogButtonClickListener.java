package ru.eva.miet.orioks.interfaces.view;

public interface OnDialogButtonClickListener {
    void onPositiveButtonClickListener();
    void onNegativeButtonClickListener();
}
