package ru.eva.miet.orioks.fragment.roles;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.auth.AuthActivity;
import ru.eva.miet.orioks.activity.splash.SplashActivity;
import ru.eva.miet.orioks.adapter.RolesFragmentAdapter;
import ru.eva.miet.orioks.interfaces.view.OnRoleItemClickListener;
import ru.eva.miet.orioks.model.orioks.RolesList;

public class RolesFragment extends Fragment implements
        ContractRoles.View,
        OnRoleItemClickListener,
        SwipeRefreshLayout.OnRefreshListener{

    private ContractRoles.Presenter mPresenter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_roles, container, false);

        Resources resources = getResources();
        int orange = resources.getColor(R.color.grade_orange);
        int yellow = resources.getColor(R.color.grade_amber);
        int lightGreen = resources.getColor(R.color.grade_lime);
        int green = resources.getColor(R.color.grade_green);

        mPresenter = new PresenterRoles(this);
        recyclerView = view.findViewById(R.id.recycler_view);

        swipeRefreshLayout = view.findViewById(R.id.container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(orange, yellow, lightGreen, green);

        mPresenter.getLocalRoles();
        return view;
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRefresh() {
        mPresenter.refreshRoles();
    }

    @Override
    public void setAdapter(List<RolesList> rolesList) {
        RolesFragmentAdapter adapter = new RolesFragmentAdapter(rolesList);
        adapter.setOnRoleItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(String role) {
        Snackbar snackbar = Snackbar
                .make(view, "Сменить роль?", Snackbar.LENGTH_LONG)
                .setAction("СМЕНИТЬ", view1 -> mPresenter.changeRole(role));
        snackbar.show();
    }

    @Override
    public void unsetRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void startSplashActivity() {
        ((Activity)view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), SplashActivity.class));
    }
    @Override
    public void finishActivity() {
        ((Activity)view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), AuthActivity.class));
    }
}