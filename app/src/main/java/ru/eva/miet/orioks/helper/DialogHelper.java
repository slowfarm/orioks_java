package ru.eva.miet.orioks.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.teacher.measure.MeasureControlActivity;
import ru.eva.miet.orioks.interfaces.view.OnDialogButtonClickListener;
import ru.eva.miet.orioks.interfaces.view.OnMeasureControlDialogButtonClickListener;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;

public class DialogHelper {

    private static volatile DialogHelper instance;

    public static DialogHelper getInstance() {
        if (instance == null)
            instance = new DialogHelper();
        return instance;
    }
    public void createDialog(String message, String title, Context context, OnDialogButtonClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setPositiveButton("Ок", (dialog, which) -> onClickListener.onPositiveButtonClickListener());
        builder.setNegativeButton("Отмена", (dialog, which) -> {
            onClickListener.onNegativeButtonClickListener();
            dialog.cancel();
        });
        builder.create().show();
    }


    public void createUserRolesDialog(Context context, OnDialogButtonClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Зайти как");
        builder.setTitle("Выберите роль");
        builder.setCancelable(false);
        builder.setPositiveButton("Студент", (dialog, which) -> onClickListener.onPositiveButtonClickListener());
        builder.setNegativeButton("Преподаватель", (dialog, which) -> {
            onClickListener.onNegativeButtonClickListener();
            dialog.cancel();
        });
        builder.create().show();
    }

    public void createValueChangerDialog(Context context, MeasureControl measureControl, OnMeasureControlDialogButtonClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Вы можете изменить оценку по мероприятию");
        builder.setTitle(measureControl.getName());
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View v = inflater.inflate(R.layout.value_changer_dialog, null);
        builder.setView(v);
        builder.setCancelable(false);
        builder.setPositiveButton("Ок", (dialog, which) -> {
            AppCompatEditText editText = v.findViewById(R.id.value_edit_text);
            onClickListener.onPositiveButtonClickListener(editText, measureControl);
        });
        builder.setNegativeButton("Отмена", (dialog, which) -> {
            dialog.cancel();
        });
        builder.create().show();
    }
}
