package ru.eva.miet.orioks.activity.main;


import android.content.Context;

import ru.eva.miet.orioks.interfaces.data.OnStudentReceived;
import ru.eva.miet.orioks.model.orioks.student.Student;
import ru.eva.miet.orioks.model.schedule.WeekType;

class ContractMain {
    interface View {

        void setPerson(String name, String number, String group);

        void setWeekType(String currentWeek, String weekType, float progress);

        void showToast(String message);

        void initStudentViews();

        void initTeacherViews();

        void initSwitchThemeItem(boolean isDarkThemeSelected);
    }

    interface Presenter {

        void checkRoles(Context context);

        void getStudent();

        void setTypeOfWeek();

        void changeTheme(Context context, boolean isChecked);

        void initSwitchThemeItem(Context context);
    }

    interface Repository {
        Student getLocalStudent();

        void getStudent(OnStudentReceived onStudentReceived);

        void getTeacher(OnStudentReceived onStudentReceived);

        void setStudent(Student student);

        WeekType getCurrentWeekType();

        String getCurrentRole();

        void setIsNightThemeSelected(Context context, boolean isSelected);

        boolean getIsDarkThemeSelected(Context context);
    }
}
