package ru.eva.miet.orioks.fragment.settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.activity.auth.AuthActivity;
import ru.eva.miet.orioks.adapter.SettingsFragmentAdapter;
import ru.eva.miet.orioks.helper.DialogHelper;
import ru.eva.miet.orioks.interfaces.view.OnDialogButtonClickListener;
import ru.eva.miet.orioks.interfaces.view.OnItemViewClickListener;
import ru.eva.miet.orioks.model.orioks.Security;


public class SettingsFragment extends Fragment implements
        ContractSettings.View,
        OnItemViewClickListener,
        SwipeRefreshLayout.OnRefreshListener{


    private ContractSettings.Presenter mPresenter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SettingsFragmentAdapter adapter;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        Resources resources = getResources();
        int orange = resources.getColor(R.color.grade_orange);
        int yellow = resources.getColor(R.color.grade_amber);
        int lightGreen = resources.getColor(R.color.grade_lime);
        int green = resources.getColor(R.color.grade_green);

        mPresenter = new PresenterSettings(this);
        recyclerView = view.findViewById(R.id.recycler_view);

        swipeRefreshLayout = view.findViewById(R.id.container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(orange, yellow, lightGreen, green);

        Button logOutBtn = view.findViewById(R.id.log_out_button);

        mPresenter.getAllActiveTokens();
        logOutBtn.setOnClickListener(v -> mPresenter.logOut(view.getContext()));
        return view;
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setAdapter(List<Security> allActiveTokens) {
        adapter = new SettingsFragmentAdapter(allActiveTokens);
        adapter.setOnItemViewClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(Security token, int position, View view) {
        Snackbar snackbar = Snackbar
                .make(view, "Завершить сессию?", Snackbar.LENGTH_LONG)
                .setAction("УДАЛИТЬ", view1 -> mPresenter.deleteActiveToken(token, position, view.getContext()));
        snackbar.show();
    }

    @Override
    public void finishActivity() {
        ((Activity)view.getContext()).finishAffinity();
        view.getContext().startActivity(new Intent(view.getContext(), AuthActivity.class));
    }

    @Override
    public void unsetRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void notifyItemRemoved(int position) {
        adapter.removeItem(position);
    }

    @Override
    public void showPusherDialog() {
        DialogHelper.getInstance().createDialog("Вы точно хотите выйти?", "Выйти", view.getContext(), new OnDialogButtonClickListener() {
            @Override
            public void onPositiveButtonClickListener() {
                mPresenter.unregisterPusher(view.getContext());
            }

            @Override
            public void onNegativeButtonClickListener() {

            }
        });
    }

    @Override
    public void onRefresh() {
        mPresenter.refreshActiveTokens();
    }
}