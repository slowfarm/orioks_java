package ru.eva.miet.orioks.interfaces.data;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.student.Event;

public interface OnEventsReceived {
    void onResponse(List<Event> eventList, int id);

    void onFailure(String message, int id);
}
