package ru.eva.miet.orioks.activity.schedule;


import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnScheduleReceived;
import ru.eva.miet.orioks.model.schedule.Schedule;
import ru.eva.miet.orioks.model.schedule.WeekType;

class RepositorySchedulerActivity implements ContractSchedulerActivity.Repository {

    @Override
    public void getSchedule(OnScheduleReceived onScheduleReceived, String group) {
        NetworkHelper.getInstance().getSchedule(group, onScheduleReceived);
    }

    @Override
    public void setSchedule(Schedule schedule) {
        StorageHelper.getInstance().setSchedule(schedule);
    }

    @Override
    public Schedule getLocalSchedule(String group) {
        return StorageHelper.getInstance().getSchedule(group);
    }

    @Override
    public WeekType getCurrentWeekType() {
        return StorageHelper.getInstance().getWeekType();
    }
}
