package ru.eva.miet.orioks.activity.schedule;


import ru.eva.miet.orioks.interfaces.data.OnScheduleReceived;
import ru.eva.miet.orioks.model.schedule.Schedule;
import ru.eva.miet.orioks.model.schedule.WeekType;

class ContractSchedulerActivity {
    interface View {

        void setPagerAdapter();

        void showToast(String text);

        void setViewPagerToPosition(int position);
    }

    interface Presenter {

        void setPagerAdapter(String group);

        void setViewPagerToCurrentWeek();
    }

    interface Repository {

        void getSchedule(OnScheduleReceived onScheduleReceived, String group);

        void setSchedule(Schedule schedule);

        Schedule getLocalSchedule(String group);

        WeekType getCurrentWeekType();
    }
}
