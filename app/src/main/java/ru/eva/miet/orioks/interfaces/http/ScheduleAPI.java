package ru.eva.miet.orioks.interfaces.http;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.eva.miet.orioks.model.schedule.Schedule;

public interface ScheduleAPI {
    @POST("/schedule/data")
    Call<Schedule> getScheduler(@Query("group") String group);

    @POST("/schedule/groups")
    Call<String[]> getGroups();
}
