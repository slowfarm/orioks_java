package ru.eva.miet.orioks.model.orioks.teacher;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Attributes extends RealmObject {
    @SerializedName("shortName")
    private String shortName;
    @SerializedName("fullName")
    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }
}
