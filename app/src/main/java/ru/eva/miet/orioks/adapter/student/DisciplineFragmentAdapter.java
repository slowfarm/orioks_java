package ru.eva.miet.orioks.adapter.student;

import android.graphics.PorterDuff;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.interfaces.view.OnDisciplineItemClickListener;
import ru.eva.miet.orioks.model.orioks.student.Discipline;


public class DisciplineFragmentAdapter extends RecyclerView.Adapter<DisciplineFragmentAdapter.ViewHolder> {

    private List<Discipline> disciplineList;
    private OnDisciplineItemClickListener onDisciplineItemClickListener;

    public DisciplineFragmentAdapter(List<Discipline> disciplineList) {
        this.disciplineList = disciplineList;
    }

    public void addItems(List<Discipline> disciplineList) {
        this.disciplineList = disciplineList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.discipline_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Discipline discipline = disciplineList.get(position);
        holder.disciplineTitleTextView.setText(discipline.getName());
        holder.rankTextView.setText(discipline.getRank());
        holder.typeTextView.setText(discipline.getFormControl());
        holder.rankTextView.getBackground().setColorFilter(discipline.getColor(), PorterDuff.Mode.DARKEN);
    }

    @Override
    public int getItemCount() {
        return disciplineList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView disciplineTitleTextView, rankTextView, typeTextView;

        ViewHolder(View itemView) {
            super(itemView);
            disciplineTitleTextView = itemView.findViewById(R.id.discipline_title_text_view);
            rankTextView = itemView.findViewById(R.id.rank_text_view);
            typeTextView = itemView.findViewById(R.id.type_text_view);
            itemView.setOnClickListener(view-> onDisciplineItemClickListener.onClick(getAdapterPosition()));
        }
    }

    public void setOnDisciplineItemClickListener(OnDisciplineItemClickListener onDisciplineItemClickListener) {
        this.onDisciplineItemClickListener = onDisciplineItemClickListener;
    }
}