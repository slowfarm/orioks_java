package ru.eva.miet.orioks.activity.teacher.measure;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import java.util.List;

import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.helper.DialogHelper;
import ru.eva.miet.orioks.interfaces.view.OnMeasureControlDialogButtonClickListener;
import ru.eva.miet.orioks.model.orioks.teacher.MeasureControl;

public class MeasureControlActivity extends AppCompatActivity implements
        ContractMeasureControl.View,
        OnMeasureControlDialogButtonClickListener {

    private ContractMeasureControl.Presenter mPresenter;
    private LinearLayout eventContainerLayout;
    private View itemEventWeek;
    private int disciplineId;
    private int groupId;
    private int studentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesure_control);
        mPresenter = new PresenterMeasureControl(this);

        eventContainerLayout = findViewById(R.id.event_container_layout);
        disciplineId = getIntent().getIntExtra("disciplineId", 0);
        groupId = getIntent().getIntExtra("groupId", 0);
        studentId = getIntent().getIntExtra("studentId", 0);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        mPresenter.setToolbarTitle(studentId);

        mPresenter.getMeasureControl(disciplineId, groupId, studentId);
    }

    @Override
    public void setToolbarTitle(String toolbarTitle) {
        AppCompatTextView disciplineTitleTextView = findViewById(R.id.discipline_title_text_view);
        disciplineTitleTextView.setText(toolbarTitle);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearListView() {
        eventContainerLayout.removeAllViews();
    }

    @Override
    public void addWeek(Integer week, List<MeasureControl> measureControlList) {
        itemEventWeek = getLayoutInflater().inflate(R.layout.item_event_week, null);
        ((AppCompatTextView) itemEventWeek.findViewById(R.id.week_text_view)).setText(week.toString());
        eventContainerLayout.addView(itemEventWeek);
        mPresenter.addMeasureControl(week, measureControlList);
    }

    @Override
    public void addEvents(MeasureControl measureControl) {
        View itemEvent = getLayoutInflater().inflate(R.layout.item_event, null);
        ((AppCompatTextView) itemEvent.findViewById(R.id.discipline_text_view)).setText(measureControl.getType());
        ((AppCompatTextView) itemEvent.findViewById(R.id.name_text_view)).setText(measureControl.getName());
        ((AppCompatTextView) itemEvent.findViewById(R.id.rank_current_text_view)).setText(measureControl.getCurrentGradeInString());
        ((AppCompatTextView) itemEvent.findViewById(R.id.rank_current_text_view)).setTextColor(measureControl.getColor());
        ((AppCompatTextView) itemEvent.findViewById(R.id.rank_max_text_view)).setText(measureControl.getMaxGradeInString());
        itemEvent.setOnClickListener(v -> DialogHelper.getInstance().createValueChangerDialog(this, measureControl, this));
        ((LinearLayout) itemEventWeek.findViewById(R.id.events_list_linear_layout)).addView(itemEvent);
    }

    @Override
    public void onPositiveButtonClickListener(AppCompatEditText editText, MeasureControl measureControl) {
        mPresenter.checkValue(editText, measureControl, disciplineId, groupId, studentId);
    }
}
