package ru.eva.miet.orioks.interfaces.view;


public interface OnGroupItemClickListener {
    void onClick(String group);
    void onShortcutAddClick(String group);
    void onLongClick(int position, String group);
}
