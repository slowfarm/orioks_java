package ru.eva.miet.orioks.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import ru.eva.miet.orioks.activity.main.MainActivity;
import ru.eva.miet.orioks.R;
import ru.eva.miet.orioks.helper.DialogHelper;
import ru.eva.miet.orioks.interfaces.view.OnDialogButtonClickListener;

/**
 * Класс авторизации.
 * @autor Владимир Сорока
 * @version 1.0
 */
public class AuthActivity extends AppCompatActivity implements ContractAuth.View {

    /** Экземпляр класса - presenter */
    private ContractAuth.Presenter mPresenter;
    /** View - кнопка авторизации */
    private Button loginBtn;
    /** View - поле ввода номера студенческого билета */
    private TextInputLayout studentNumberLayout;
    /** View - поле ввода пароля */
    private TextInputLayout passwordLayout;

    private TextInputEditText studentNumberTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mPresenter = new PresenterAuth(this);

        studentNumberTextView = findViewById(R.id.student_number_text_view);
        TextInputEditText passwordTextView = findViewById(R.id.password_text_view);
        studentNumberLayout = findViewById(R.id.student_number_layout);
        passwordLayout = findViewById(R.id.password_layout);
        loginBtn = findViewById(R.id.log_in_button);

        studentNumberTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                mPresenter.onStudentNumberTextChanged(s);
            }
        });
        passwordTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count){}
            @Override
            public void afterTextChanged(Editable s){
                mPresenter.onPasswordTextChangeListener(s);
            }
        });

        loginBtn.setOnClickListener(v-> mPresenter.onLoginButtonClick(studentNumberTextView.getText(), passwordTextView.getText(), this));

        mPresenter.setLogin(this);
    }


    /**
     * Процедура перехода на новый экран
     */
    @Override
    public void startActivity() {
        startActivity(new Intent(AuthActivity.this, MainActivity.class));
        finish();
    }

    /**
     * Процедура вывода на toast на экран
     * @param text - текст сообщения
     */
    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Процедура активации/деактивации кнопки {@link AuthActivity#loginBtn}
     * @param isEnable - конфигурация
     */
    @Override
    public void setLoginButtonEnable(boolean isEnable) {
        loginBtn.setEnabled(isEnable);
    }

    /**
     * Процедура вывода ошибки при вводе номера студенческого билета в поле {@link AuthActivity#studentNumberLayout}
     * @param error - текст ошибки
     */
    @Override
    public void setStudentNumberLayoutError(String error) {
        studentNumberLayout.setError(error);
    }
    /**
     * Процедура вывода ошибки при вводе пароля в поле {@link AuthActivity#passwordLayout}
     * @param error - текст ошибки
     */
    @Override
    public void setLoginLayoutError(String error) {
        passwordLayout.setError(error);
    }

    @Override
    public void setLogin(String login) {
        studentNumberTextView.setText(login);
    }

    @Override
    public void showPusherDialog() {
        DialogHelper.getInstance().createDialog("Разрешить отправку Push-уведомлений?", "Разрешить", this, new OnDialogButtonClickListener() {
            @Override
            public void onPositiveButtonClickListener() {
                mPresenter.allowPushService(getApplicationContext());
            }

            @Override
            public void onNegativeButtonClickListener() {
                startActivity();
            }
        });
    }
}
