package ru.eva.miet.orioks.interfaces.data;

import java.util.List;

import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;

public interface OnTeacherEventsReceived {
    void onResponse(List<TeacherEvent> disciplineList);
    void onFailure(String message);
}
