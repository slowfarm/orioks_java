package ru.eva.miet.orioks.fragment.teacher.event;

import com.google.android.gms.auth.api.signin.internal.Storage;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnSemestersReceived;
import ru.eva.miet.orioks.interfaces.data.OnTeacherEventsReceived;
import ru.eva.miet.orioks.interfaces.data.OnTokenReceived;
import ru.eva.miet.orioks.model.orioks.Semester;
import ru.eva.miet.orioks.model.orioks.SemesterList;
import ru.eva.miet.orioks.model.orioks.teacher.StudyGroup;
import ru.eva.miet.orioks.model.orioks.teacher.TeacherEvent;


class RepositoryEvent implements ContractEvent.Repository {

    @Override
    public List<TeacherEvent> getLocalTeacherEventList() {
        return StorageHelper.getInstance().getTeacherEventList();
    }

    @Override
    public void getTeacherEventList(OnTeacherEventsReceived onTeacherEventsReceived) {
        NetworkHelper.getInstance().getTeacherEvents(StorageHelper.getInstance().getAccessToken(), onTeacherEventsReceived);
    }

    @Override
    public void setTeacherEventList(List<TeacherEvent> teacherEventList) {
        StorageHelper.getInstance().setTeacherEventList(teacherEventList);
    }

    @Override
    public StudyGroup getStudyGroup(int groupId) {
        return StorageHelper.getInstance().getStudyGroup(groupId);
    }

    @Override
    public Semester getLocalSemester() {
        return StorageHelper.getInstance().getSemester();
    }

    @Override
    public void changeSemester(Integer id, OnSemestersReceived onSemestersReceived) {
        NetworkHelper.getInstance().changeSemester(StorageHelper.getInstance().getAccessToken(), id, onSemestersReceived);
    }

    @Override
    public void clearDisciplines() {
        StorageHelper.getInstance().clearDisciplines();
    }

    @Override
    public void clearAllTables() {
        StorageHelper.getInstance().clearTables();
    }

    @Override
    public void deleteToken(OnTokenReceived onTokenReceived) {
        NetworkHelper.getInstance().deleteAccessToken(StorageHelper.getInstance().getAccessToken(), -1, onTokenReceived);
    }
}
