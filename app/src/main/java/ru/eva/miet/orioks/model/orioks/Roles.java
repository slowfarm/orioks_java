package ru.eva.miet.orioks.model.orioks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Roles extends RealmObject {

    @SerializedName("current")
    @Expose
    private String current;
    @SerializedName("parent")
    @Expose
    private String parent;
    @SerializedName("list")
    @Expose
    private RealmList<RolesList> list = null;

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public RealmList<RolesList> getList() {
        return list;
    }

    public void setList(RealmList<RolesList> list) {
        this.list = list;
    }

}