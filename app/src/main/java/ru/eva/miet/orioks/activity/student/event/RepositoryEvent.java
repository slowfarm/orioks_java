package ru.eva.miet.orioks.activity.student.event;

import java.util.List;

import ru.eva.miet.orioks.helper.domain.NetworkHelper;
import ru.eva.miet.orioks.helper.domain.StorageHelper;
import ru.eva.miet.orioks.interfaces.data.OnEventsReceived;
import ru.eva.miet.orioks.model.orioks.student.Discipline;
import ru.eva.miet.orioks.model.orioks.student.Event;

class RepositoryEvent implements ContractEvent.Repository {

    @Override
    public String getToolbarTitle(int id) {
        return StorageHelper.getInstance().getDiscipline(id).getName();
    }

    @Override
    public Discipline getDiscipline(int id) {
        return StorageHelper.getInstance().getDiscipline(id);
    }

    @Override
    public List<Event> getEvents(int id) {
        return StorageHelper.getInstance().getEventsList(id);
    }

    @Override
    public void getEventList(int id, OnEventsReceived onEventsReceived) {
        NetworkHelper.getInstance().getEvents(StorageHelper.getInstance().getAccessToken(), id, onEventsReceived);
    }

    @Override
    public void setEventList(List<Event> eventsList, int id) {
        StorageHelper.getInstance().setEvents(eventsList, id);
    }
}
