package ru.eva.miet.orioks.fragment.schedulepager.secondnumerator;

import java.util.List;

import ru.eva.miet.orioks.helper.ConvertHelper;
import ru.eva.miet.orioks.model.schedule.Data;


public class PresenterSecondNumeratorFragment implements ContractSecondNumeratorFragment.Presenter{
    private ContractSecondNumeratorFragment.View mView;
    private ContractSecondNumeratorFragment.Repository mRepository;


    PresenterSecondNumeratorFragment(ContractSecondNumeratorFragment.View mView) {
        this.mView = mView;
        mRepository = new RepositorySecondNumeratorFragment();
    }

    @Override
    public void getSchedule(String group) {
        List<Data> dataList = ConvertHelper.getInstance().scheduleWeek(mRepository.getSchedule(2, group));
        mView.setAdapter(dataList);
    }
}
