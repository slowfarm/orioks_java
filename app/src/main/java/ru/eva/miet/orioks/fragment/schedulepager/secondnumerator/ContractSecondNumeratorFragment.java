package ru.eva.miet.orioks.fragment.schedulepager.secondnumerator;

import java.util.List;

import ru.eva.miet.orioks.model.schedule.Data;


class ContractSecondNumeratorFragment {

    interface View {

        void setAdapter(List<Data> schedule);
    }

    interface Presenter {

        void getSchedule(String group);
    }

    interface Repository {

        List<Data> getSchedule(int week, String group);
    }
}
